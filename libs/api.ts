import axios from "axios";
const BASEURL = process.env.NEXT_PUBLIC_NODE_ENV == 'dev' ? process.env.NEXT_PUBLIC_STRAPI_DEV_API_URL : process.env.NEXT_PUBLIC_STRAPI_PROD_API_URL


export async function fetchAll(data: any, page:any, pageSize:any) {
  console.log(data, "Dadadada api");

  const url =
    data == ""
      ? `${BASEURL}/products`
      : `${BASEURL}/products?${data}`;

  // const urlNew = `${BASEURL}/products?${data}`;
  const data1: any = await fetch(url);
  try {
    return await data1.json();
  } catch (error) {
    return error;
  }
}

export async function fetchAllPaging(data: any, page:any, pageSize:any) {
  console.log(data, "Dadadada api");
  const url =
    data == ""
      ? `${BASEURL}/products?pagination[page]=${page}&pagination[pageSize]=${pageSize}`
      : `${BASEURL}/products?${data}&pagination[page]=${page}&pagination[pageSize]=${pageSize}`;
  const data1: any = await fetch(url);
  try {
    return await data1.json();
  } catch (error) {
    return error;
  }
}


export async function fetchSearch(data: any,) {
  console.log(data, "Dadadada api");
  const url =
    data == ""
      ? `${BASEURL}/products`
      : `${BASEURL}/products?${data}`;
  const data1: any = await fetch(url);
  try {
    return await data1.json();
  } catch (error) {
    return error;
  }
}


export async function createSlug(data: any,) {
  console.log(data, "Dadadada api");
  const url =
    data == ""
      ? `${BASEURL}/products`
      : `${BASEURL}/products?${data}`;
  const data1: any = await fetch(url);
  try {
    return await data1.json();
  } catch (error) {
    return error;
  }
}


export async function wishCountHandler(body: any, id: any) {
  console.log(body, "count+1");

  return await axios.put(
    `${BASEURL}/products/${id}`,

    { data: { wishListCount: body } }
  );
}


export async function getOne( filter: any) {
  const url = `${BASEURL}/products?${filter}`;

  try {
    const response = await axios.get(url);
    return  response;
  } catch (error) {
    return error;
  }
}
