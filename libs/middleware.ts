
export default [
    // The array is pre-populated with internal, built-in middlewares, prefixed by `strapi::`
    'strapi::cors',
    'strapi::body',
    'strapi::errors',
    // ...
    'my-custom-node-module', // custom middleware that does not require any configuration
    {
      // custom name to find a package or a path
      name: 'my-custom-node-module',
      config: {
        origin: ["*"],
        methods: ["GET", "POST", "PUT", "PATCH", "DELETE", "HEAD", "OPTIONS"],
        headers: ["Content-Type", "Authorization", "Origin", "Accept"],
        keepHeaderOnError: true,
      },
    },
    {
      // custom resolve to find a package or a path
      resolve: '../some-dir/custom-middleware',
      config: {
        foo: 'bar',
      },
    },
  ];