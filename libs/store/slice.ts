// import { FilterationSystem } from "@/utils/common";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import qs from "qs"
import Cookies from "js-cookie";
import { fetchAll, getOne, wishCountHandler,fetchSearch,createSlug } from "../api";


export const initialState = { fetchAll };
console.log(initialState, "ini");


export const getCompareList = createAsyncThunk("comparelist",async (data:any) => {
  try{

    const CompareData = JSON.parse(Cookies.get("compare"))
    return CompareData
  }catch(e){
    return e
  }
})

export const getWishList = createAsyncThunk("wishlist",async (data:any) => {
  try{

    const CompareData = JSON.parse(Cookies.get("wishlist"))
    return CompareData
  }catch(e){
    return e
  }
})

export const getRecentlyList = createAsyncThunk("recentlist",async (data:any) => {
  try{

    const CompareData = JSON.parse(Cookies.get("recently"))
    return CompareData
  }catch(e){
    return e
  }
})

export const GetDetails = createAsyncThunk("details", async (data: any) => {
  try{
    const filter = qs.stringify({
      filters:{
        $and:[
          {
            Slug: {$eq: data.slug}
          },
          {
            MachineSerial: {$eq: data.serialno}
          }
        ]
      }
    })
    const response = await getOne(filter)
    return response.data
  }catch(e){
    return e
  }
})

export const getdata = createAsyncThunk(
  "list",
  async (data: any, { getState }) => {

    console.log(data, "FILTERSsss");
    

    try {
      const query = qs.stringify({
        filters: {
          $and: [
            {
              $or: [{ Industries: { $in: data.industryFilter } }],
            },
            {
              $or: [
                {
                  EquipmentType: { $in: data.EquipmentFilter },
                },
              ],
            },
            {
              $or: [
                {
                  SubEquipmentType: { $in: data.SubEquipmentFilter },
                },
              ],
            },
            {
              $or: [
                {
                  Brand: { $in: data.BrandFilter },
                },
              ],
            },
            {
              $or: [
                {
                  ManufactureDate: { $between: data.yearFilter },
                },
              ],
            },
            {
              $or: [
                {
                  SaleType: { $in: data.saleTypeFilter },
                },
              ],
            },
            {
              $or: [
                {
                  Status: { $in: data.StatusFilter },
                },
              ],
            },
            {
              $or: [
                {
                  Type: { $in: data.tagFilter },
                },
              ],
            },
            {
              $or: [
                {
                  Operatingweight: { $between: data.WeightFilter },
                },
              ],
            },
            {
              $or: [
                {
                  Workinghours: { $between: data.HoursFilter },
                },
              ],
            },
            {
              $or: [
                {
                  Mileage: { $between: data.MileageFilter },
                },
              ],
            },
            ,
            {
              $or: [
                {
                  Price: { $between: data.PriceFilter },
                },
              ],
            },
          ],
        },
        populate: "*",
      });
      // console.log(filterData,"filterData")

      console.log(query, data.page, data, "!!!TEST");
      

      const response = await fetchAll(query, 1, data.pageSize);


      console.log(response, "vinay323454");
      const newresponse = await response;
      let obj;
      if (
        data.page == 1 ||
        data.industryFilter.length > 0 ||
        data.EquipmentFilter.length > 0 ||
        data.SubEquipmentFilter.length > 0 ||
        data.BrandFilter.length > 0 ||
        data.yearFilter.length > 0 ||
        data.saleTypeFilter.length > 0 ||
        data.StatusFilter.length > 0 ||
        data.tagFilter.length > 0 ||
        data.WeightFilter.length > 0 ||
        data.HoursFilter.length > 0 ||
        data.MileageFilter.length > 0
      ) {
        obj = {
          new: false,
          response: newresponse,
        };
      } else {
        obj = {
          new: true,
          response: newresponse,
        };
      }
      return obj;
    } catch (e) {
      return e.response;
    }
  }
);

export const filterSearch = createAsyncThunk(
  "Search",
  async (data: any) => {
    try {
      const filter = data  !== "" ? `Title_contains=${data}&Description_contains=${data}` : "";
     const response = await fetchSearch(filter);
    // console.log(response, "Brand");
      console.log(response,"search")
      return response
    } catch (e) {
      return e.response;
    }
  }
);

export const Slug = createAsyncThunk(
  "Search",
  async (data: any) => {
    try {
      const filter = data  !== "" ? `filters\[Slug\][$eq]${data.Slug}` : "";
     const response = await createSlug(filter);
    // console.log(response, "Brand");
      console.log(response,"search")
      return response
    } catch (e) {
      return e.response;
    }
  }
);

export const filterRecent = createAsyncThunk("Brandlist", async (data: any) => {
  try {
    const filter = data !== "" ? `filters[CreatedAt][$eq]=${data}` : "";
    const response = await fetchSearch(filter);
    console.log(response, "recents");
   return await response;
  } catch (e) {
    return e.response;
  }
});

export const filterManufactureDate = createAsyncThunk(
  "ManufactureDatelist",
  async (data: any) => {
    try {
      const filter =
        data !== "DATE" ? `filters[ManufactureDate][$eq]=${data}` : "";
      // const response = await fetchAll(filter);
      // console.log(response, "Brand");
      // return await response;
    } catch (e) {
      return e.response;
    }
  }
);

export const share = createAsyncThunk("share", async (data: any) => {
  try {
    console.log(data, "Peoples1");
    const response = await wishCountHandler(data?.wishListCount, data?.id);
    console.log(response, "Megan");

    return await response;
  } catch (e) {
    return e.response;
  }
});

export const dataSlice = createSlice({
  name: "catalog",
  initialState: {
    cataloglist: [],
    catalogerr: "",
    catalogmeta: {},
    serialno:"",
    slug:'',
    tag: "",
    compareData: [],
    compareerr: "",
    wishData: [],
    recentlyData: [],
    construction: false,
    catlogDetail: {},
    quarry: false,
    mining: false,
    oil: false,
    sales: false,
    buldozer: false,
    Crawler: false,
    Mini: false,
    offroad: false,
    excavators: false,
    hasmore: true,
  },
  reducers: {

    filterdata: (state, action) => {
      const filteredData = state.cataloglist.filter(
        (cat) => cat.attributes.Type == action.payload
      );
      console.log(action.payload, filteredData, "payloaddd");
      state.tag = action.payload;
      state.cataloglist = state.cataloglist.filter(
        (cat) => cat.attributes.Type == action.payload
      );
    },

    setQueryParameter: (state, action) =>{
      state.slug = action.payload.slug
      state.serialno = action.payload.serialno
    },

      // not used
     filterRecentdata: (state, action) => {
      const filterdata = state.cataloglist.filter(
        (cat) => cat.attributes.CreatedAt == Date.now()
      );
      state.cataloglist = filterdata;
    },

    // not used
    setCheckFilterEnable: (state, action) => {
      console.log(action.payload, "payloadd");
      if (action.payload == "construction") {
        if (state.construction == true) {
          state.construction = false;
        } else {
          state.construction = true;
        }
      }
      if (action.payload == "quarry") {
        console.log("quarry");
        if (state.quarry == true) {
          state.quarry = false;
        } else {
          state.quarry = true;
        }
      }
      if (action.payload == "mining") {
        console.log("mining");

        if (state.mining == true) {
          state.mining = false;
        } else {
          state.mining = true;
        }
      }
      if (action.payload == "oil") {
        console.log("oil");

        if (state.oil == true) {
          state.oil = false;
        } else {
          state.oil = true;
        }
      }
      if (action.payload == "buldozer") {
        console.log("oil");

        if (state.buldozer == true) {
          state.buldozer = false;
        } else {
          state.buldozer = true;
        }
      }
      if (action.payload == "crawler") {
        console.log("oil");

        if (state.Crawler == true) {
          state.Crawler = false;
        } else {
          state.Crawler = true;
        }
      }
      if (action.payload == "mini") {
        console.log("oil");

        if (state.Mini == true) {
          state.Mini = false;
        } else {
          state.Mini = true;
        }
      }
      if (action.payload == "offroad") {
        console.log("oil");

        if (state.offroad == true) {
          state.offroad = false;
        } else {
          state.offroad = true;
        }
      }
      if (action.payload == "excavators") {
        console.log("oil");

        if (state.excavators == true) {
          state.excavators = false;
        } else {
          state.excavators = true;
        }
      }
      // state.construction = true
      // console.log(action.payload,"pay;oad")
      // action.payload == 'construction' ? state.construction =true : action.payload == 'quarry' ? state.quarry == true ? false : true : action.payload == 'mining' ? state.mining == true ? false : true : action.payload == 'oil' ? state.oil == true ? false : true :""
    },

    //not used
    setTabData: (state, action) => {
      const filterdata = state.cataloglist.filter(
        (cat) => cat.attributes.SaleType == action.payload.toUpperCase()
      );
      state.cataloglist = filterdata;
    },

    setHasMore: (state, action) => {
      state.hasmore = true;
    },

    //not used
    setIndustriesData: (state, action) => {
      const filterdata = state.cataloglist.filter(
        (cat) => cat.attributes.Industries == action.payload.toUpperCase()
      );
      state.cataloglist = filterdata;
    },
    //not used
    setEquipmentData: (state, action) => {
      const filterdata = state.cataloglist.filter(
        (cat) => cat.attributes.EquipmentType == action.payload.toUpperCase()
      );
      state.cataloglist = filterdata;
    },
    //not used
    setBrandData: (state, action) => {
      const filterdata = state.cataloglist.filter(
        (cat) => cat.attributes.Brand == action.payload.toUpperCase()
      );
      state.cataloglist = filterdata;
    },
    //not used
    setManufactureDate: (state, action) => {
      const filterdata = state.cataloglist.filter(
        (cat) => cat.attributes.ManufactureDate == action.payload.toUpperCase()
      );
      state.cataloglist = filterdata;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getdata.fulfilled, (state: any, action: any) => {
      console.log("!!!getdata.fulfilled!!!");
      console.log(action.payload, action, "payloadd");
      if (action.payload && action.payload.new == false) {
        state.cataloglist = action.payload.response.data;
        state.catalogmeta = action.payload.response.meta;
        state.hasmore = false;
      } else if (action.payload && action.payload.new == true) {
        state.cataloglist = [
          ...state.cataloglist,
          ...action.payload.response.data,
        ];
        state.hasmore = false;
      }
      if (action.payload && action.payload.response && action.payload.response.data && action.payload.response.data.length == 0) {
        state.hasmore = false;
      }
    }),
      builder.addCase(getdata.rejected, (state: any, action: any) => {
        state.catalogerr = action.payload;
      }),
      builder.addCase(filterSearch.fulfilled, (state: any, action: any) => {
        console.log(action, "action");
        state.cataloglist = action.payload.data;
        state.catalogmeta = action.payload.meta;
      }),
      builder.addCase(filterSearch.pending, (state: any, action: any) => {
        state.catalogerr = action.payload;
      }),
      builder.addCase(filterRecent.fulfilled, (state: any, action: any) => {
        console.log(action, "action");
        state.cataloglist = action.payload.data;
        state.catalogmeta = action.payload.meta;
      }),
      builder.addCase(filterRecent.pending, (state: any, action: any) => {
        state.catalogerr = action.payload;
      }),
      builder.addCase(GetDetails.fulfilled, (state: any, action: any) => {
        console.log(action, "action");
        state.catlogDetail = action.payload.data;
      }),
      builder.addCase(GetDetails.pending, (state: any, action: any) => {
        state.catalogerr = action.payload;
      }),
     
      builder.addCase(getCompareList.fulfilled, (state, action) => {
        state.compareData = action.payload;
      });
    builder.addCase(getCompareList.rejected, (state, action) => {
      // state.compareerr = action.payload;
    });
    builder.addCase(getRecentlyList.fulfilled, (state, action) => {
      state.recentlyData = action.payload;
    });
    builder.addCase(getRecentlyList.rejected, (state, action) => {
      // state.compareerr = action.payload;
    });
    builder.addCase(getWishList.fulfilled, (state, action) => {
      state.wishData = action.payload;
    });
    
      builder.addCase(
        filterManufactureDate.fulfilled,
        (state: any, action: any) => {
          console.log(action, "action");
          state.cataloglist = action.payload.data;
          state.catalogmeta = action.payload.meta;
        }
      ),
      builder.addCase(
        filterManufactureDate.pending,
        (state: any, action: any) => {
          state.catalogerr = action.payload;
        }
      );
  },
});
export const {
  filterdata,
  setIndustriesData,
  setEquipmentData,
  setBrandData,
  setManufactureDate,
  setCheckFilterEnable,
  setHasMore,
  filterRecentdata,
  setQueryParameter
} = dataSlice.actions;

export default dataSlice.reducer;
