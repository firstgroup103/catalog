import { configureStore } from "@reduxjs/toolkit";
import  dataSlice  from "./slice";
 const store= configureStore({
	reducer: {
	   catalog: dataSlice
	   
	}
 })
 
export default store