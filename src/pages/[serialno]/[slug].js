import CatalogDetails from "../../../components/CatalogDetails";
import Layout from "../../../components/common/layout";

// const inter = Inter({ subsets: ['latin'] })

export default function HomePage() {
  return (
    <>
      <Layout>
        <CatalogDetails />
      </Layout>
    </>
  );
}
