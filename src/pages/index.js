import Layout from "../../components/common/layout";
import Landing from "./landing";
import {SSRProvider} from '@react-aria/ssr'; 
// const inter = Inter({ subsets: ['latin'] })

export default function HomePage() {
  return (
    <>
    <SSRProvider>
      <Layout>
        <Landing />
      </Layout>
      </SSRProvider>
    </>
  );
}
