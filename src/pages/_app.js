import React, { useEffect } from "react";
import "@/styles/globals.css";
import "@/styles/landing.css";
import "@/styles/footer.css";
import "@/styles/catalog.css";
import { Provider } from 'react-redux'; 
import { Store } from "redux";
import "@/styles/responsive.css";
import {SSRProvider} from '@react-aria/ssr'; 
import "@/styles/catalogdetails.css";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import AOS from "aos";
import "aos/dist/aos.css";
import store from "../../libs/store/store";
// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";

export default function App({ Component, pageProps }) {
  useEffect(() => {
    AOS.init({ duration: 1200 });
  }, []);
  return <Provider store={store}><Component {...pageProps} /><ToastContainer /></Provider>;
}
