import Catalog from "../../../components/Catalog";
import Layout from "../../../components/common/layout";
import Footer from "../../../components/common/footer";
// const inter = Inter({ subsets: ['latin'] })
import { SSRProvider } from "@react-aria/ssr";
export default function HomePage() {
  return (
    <>
      <Layout>
        <Catalog />
       
      </Layout>
    </>
  );
}
