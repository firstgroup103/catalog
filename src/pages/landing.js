import Slider from "react-slick";
import Button from "react-bootstrap/Button";
import Footer from "../../components/common/footer";
const Landing = () => {
  var settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
  };
  return (
    <>
      <div className="landing-page " id="home">
        <div className="logo ">
          <img src="/assets/images/gelenlogo.png" />
        </div>

        <div>
          <div className="bg-image">
            {" "}
            <div className="upper-text">
              <h4>Gelen Industrial Solutions —</h4>
              <h4>Worldwide supplier</h4>
              <h4>Heavy machinery | Equipment & Parts</h4>
            </div>
          </div>
          <div className="mobilebg-image">
            {" "}
            <div className="upper-text">
              <h4>
                Gelen Industrial
                <br /> Solutions —
              </h4>
              <h4>Worldwide supplier</h4>
              <h4>
                Heavy machinery |<br /> Equipment & Parts
              </h4>
            </div>
          </div>
        </div>

        <div className="container">
          <div className="about mt-4">
            <h3 className="py-2">About</h3>
            <div className="row">
              <div className="col-md-7 col-sm-7" data-aos="fade-right">
                <p>
                  Gelen Industrial Solutions, Inc. was established as a branch
                  Gelen, FZE service division to provide a unique service and
                  end-to-end solutions based on the experience of cooperation
                  with participants throughout the entire chain.
                </p>
                <p>
                  We provide for our manufacturing partners a wide range of
                  equipment, new and spare parts for the industrial market and
                  full range of services, reducing the sales price and offering
                  financial support to the companies when spare parts, materials
                  and components are required but can’t be fully paid in time.
                  We continuously expand the network of contacts and optimize
                  your time, operating costs and even risks associated with the
                  unpredictability of non-branded parts and the lack of
                  technical support from alternative suppliers.
                </p>
                <p>
                  Gelen Industrial Solutions, Inc. supports the delivery process
                  to customers, bridging the gap between the demand for spare
                  parts and suppliers & manufacturers all over the world.{" "}
                </p>
              </div>
              <div className="col-md-5 col-sm-5" data-aos="fade-left">
                <div className="jcb-image d-flex justify-content-center align-items-center">
                  <img src="/assets/images/img1.png" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="part-text">
          <p>
            Substantial savings. Quality parts. Exceptional and fast service.
            <br />
            These are just a few of the things we strive for every single day,
            on every single order.{" "}
          </p>
        </div>
        <div className="logistic container">
          <h3 className="py-2">Logistic</h3>
          <div className="row mt-4">
            <div className="col-sm-5 col-md-5" data-aos="fade-right">
              <div>
                <img src="/assets/images/img2.png" />
              </div>
            </div>
            <div
              className="col-sm-7 col-md-7 logistic-content"
              data-aos="fade-left"
            >
              <p>
                Gelen Industrial Solutions, Inc. promotes products to create a
                mutually successful cooperation. The success in international
                relationships is the result of actions. We aim to deliver your
                order in the shortest possible time frame. We provide for our
                manufacturing partners with a full range of services that ensure
                the deal is correctly fulfilled, while providing access to sales
                markets and reducing the sales price of goods.
              </p>
              <p>
                We carry out projects on tailored optimization, arrange
                financial support on special conditions, assume potential
                currency risks as well as other risks related to export, transit
                and regulatory requirements, packaging and other factors.
              </p>
            </div>
          </div>
        </div>
        <div className="brand-bg">
          <p>
            Fully-Serviced, Quality-Proved Equipment & Parts From The World’s
            Top Brands{" "}
          </p>
        </div>
        <div className="mobilebrand-bg">
          <p>
            Fully-Serviced, Quality-Proved Equipment & Parts From The World’s
            Top Brands{" "}
          </p>
        </div>
        <div className="equipment container" data-aos="zoom-in">
          <h3 className="py-2">Equipment</h3>
          <div className="equip-content">
            <p>
              Gelen Industrial Solutions, Inc.able to supply worldwide and offer
              an extensive range of machinery, parts (new, remanufactured and
              used), find suitable repair and servicing solutions. Gelen
              Industrial Solutions, Inc. provides extensive industry knowledge
              and problem solving experience. We are able to find the best
              solution for clients’ needs.
            </p>
            <p>
              Our equipment is professionally maintained for maximum
              performance, backed by a team of well experienced mechanics &
              tenured operators who know the value of your project’s uptime.{" "}
            </p>
            <p>Browse available equipment or send the request.</p>
          </div>
          <div className="mt-5 row">
            <div className="col-sm-3 col-md-3 mt-2">
              <div className="d-flex justify-content-center align-items-center flex-column">
                <img src="/assets/images/pic1.png" />
                <p>Mining</p>
              </div>
            </div>
            <div className="col-sm-3 col-md-3 mt-3">
              <div className="d-flex justify-content-center align-items-center flex-column">
                <img src="/assets/images/pic2.png" style={{ width: "69%" }} />
                <p>Oil and Gas Industry</p>
              </div>
            </div>{" "}
            <div className="col-sm-3 col-md-3">
              <div className="d-flex justify-content-center align-items-center flex-column">
                <img src="/assets/images/pic3.png" />
                <p>Road and Construction</p>
              </div>
            </div>{" "}
            <div className="col-sm-3 col-md-3 mt-4">
              <div className="d-flex justify-content-center align-items-center flex-column">
                <img src="/assets/images/pic4.png" />
                <p>Energy</p>
              </div>
            </div>
          </div>
        </div>
        <div className="brands container" data-aos="zoom-in-up">
          <h3 className="py-2">Brands</h3>
          <div className="brand-content mb-4">
            <p>
              Gelen Industrial Solutions, Inc. respects partners' policy and
              provides a methodical, professional approach, which helps to
              protect customers at every stage of cooperation.
            </p>
          </div>

          <div className="brand-name d-flex justify-content-between flex-row">
            <div className="">
              <img src="/assets/images/a1.png" />
            </div>
            <div className="">
              <img src="/assets/images/a2.png" />
            </div>{" "}
            <div className="">
              <img src="/assets/images/a3.png" />
            </div>{" "}
            <div className="">
              <img src="/assets/images/a4.png" />
            </div>{" "}
            <div className="">
              <img src="/assets/images/a5.png" />
            </div>{" "}
            <div className="">
              <img src="/assets/images/a6.png" />
            </div>{" "}
            <div className="">
              <img src="/assets/images/a7.png" />
            </div>{" "}
            <div className="">
              <img src="/assets/images/a8.png" />
            </div>{" "}
            <div className="">
              <img src="/assets/images/a9.png" />
            </div>
          </div>
          <div className="brand-name d-flex justify-content-between flex-row ">
            <div className="">
              <img src="/assets/images/a10.png" />
            </div>{" "}
            <div className="">
              <img src="/assets/images/a11.png" />
            </div>{" "}
            <div className="">
              <img src="/assets/images/a12.png" />
            </div>{" "}
            <div className="">
              <img src="/assets/images/a13.png" />
            </div>
            <div className="">
              <img src="/assets/images/a14.png" />
            </div>{" "}
            <div className="">
              <img src="/assets/images/a15.png" />
            </div>
          </div>
        </div>
        <div className="service-bg">
          <p>
            Quality, effectiveness and promptness are our three main objectives
            <br />
            in order to achieve our main goal: obtaining our customers’
            satisfaction.
          </p>
        </div>
        <div className="mobileservice-bg">
          <p>
            Quality, effectiveness and promptness are our three main objectives
            <br />
            in order to achieve our main goal: obtaining our customers’
            satisfaction.
          </p>
        </div>
        <div className="customer-list container">
          <h3 className="py-2">Customer List</h3>
          <div className="row mt-4">
            <div
              className="col-sm-4 col-md-4 logistic-content"
              data-aos="fade-left"
            >
              <p>
                <strong>GIS professional expertise</strong> is confirmed by many
                years of experience in companies — industry leaders. Our
                customer list is always updated: we do not stop expanding from
                regulations to techniques required to satisfy the customers’
                needs in the best way.
              </p>
            </div>
            <div className="col-sm-8 col-md-8" data-aos="fade-right">
              <div>
                <img src="/assets/images/img5.png" style={{ width: "100%" }} />
              </div>
            </div>
          </div>
        </div>
        <div className="price container">
          <h3 className="py-2">Price</h3>
          <div className="row mt-4 logistic-content">
            <div className="col-sm-6 col-md-6 " data-aos="fade-left">
              <p>
                <strong> GIS Customer-Oriented Pricing </strong>is not about
                'brand value'. We do not deviate from industry-wide quality
                standards and constantly upgrade and improve business processes
                in order to provide customers with access to spare parts at a
                fair price with consistent quality and a high level of service.
                We ship parts worldwide and support partners in service, sales,
                logistics, quality, fast delivery, fair pricing. We grow
                together and continuously.
              </p>
            </div>
            <div className="col-sm-6 col-md-6" data-aos="fade-right">
              <div>
                <p>
                  For partnering opportunities and terms, please contact us.
                </p>
                <div className="d-flex justify-content-center align-items-center mt-4">
                  {" "}
                  <Button className="btn-contact">Contact us</Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};
export default Landing;
