import React from "react";
import MainHead from "../common/mainhead";
// import "react-toastify/dist/ReactToastify.min.css";

export default function Layout(props) {
  return (
    <>
      <MainHead title={props.title} />
      {/* <Header/> */}

      {props.children}
    </>
  );
}
