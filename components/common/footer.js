const Footer = () => {
  return (
    <>
      <div className="footer">
        <div className="container">
          <div className="footer-container">
            <div className="footer-copyright">
              <div className="footer-img"><img src="/assets/images/footer.png" /></div>
              <div className="footer-copy">Copyright © 2023 Gelen Industrial Solutions, Ltd.</div>
            </div>
            <div className="footer-address">
              <p>Brunch Co. Gelen</p>
              <p>Shariah, United Arab Emirates</p>
              <p>Saif Office (Q1-07-052/A)</p>
              <p>+971 58 175 5868</p>
              <p>Brunch Co. Gelen</p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Footer;
