/* eslint-disable react/jsx-key */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */
import { useEffect, useState } from "react";

import {
  DropdownButton,
  Dropdown,
  Form,
  Navbar,
  Nav,
  Container,
  Row,
  Col,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import Modal from 'react-bootstrap/Modal';
import Link from "next/link";

import Cookies from "js-cookie";
import { useDispatch, useSelector } from "react-redux";
import { getWishList,filterSearch } from "../../libs/store/slice";
import { useRouter } from "next/router";
import Drawer from "react-modern-drawer";
import { useWindowSize } from "@/utils/usewindowsize";
import { RemoveWishCookie } from "@/utils/common";

const CatalogHeader = ({ setFilter, onOpenModal2 }) => {
  const [open, setOpen] = useState(false);

  const { width } = useWindowSize();
  const wish =
    typeof window !== "undefined" &&
    Cookies.get("wishlist") &&
    JSON.parse(Cookies.get("wishlist"));
  const [wishList, setWishList] = useState(wish || []);
  const dispatch = useDispatch();
  const { wishData } = useSelector((state) => state.catalog);
  const onOpenModal = () => setOpen(true);
  const onCloseModal = () => setOpen(false);
  const [draweropen, setDrawerOpen] = useState(false);
  console.log(wishList, "Cookies.get('wishlist')");
  useEffect(() => {
    async function fetchWishList() {
      if (Cookies.get("wishlist") != undefined) {
        dispatch(getWishList());
      }
    }
    fetchWishList();
  }, []);
  const CheckWishListProduct = (id) => {
    const Data = wishData.find((da) => da.id == id);
    console.log(Data, "dadad");
    if (Data !== undefined && Object.keys(Data).length > 0) {
      return true;
    } else {
      false;
    }
  };
  const router = useRouter();
  console.log(router.pathname, "pathname");
  return (
    <>
      <div
        className={router.query.serialno ? "catalog-header2" : "catalog-header"}
      >
        <Navbar collapseOnSelect expand="xl">
          <Link href="/">
            <Navbar.Brand>
              <img
                style={{ width: "190px;", marginRight: " 1.4rem" }}
                src="/assets/images/head.png"
              />
            </Navbar.Brand>
          </Link>
          <img
            className="menu-mobile"
            src="/assets/images/Menu.png"
            onClick={() => setDrawerOpen(!draweropen)}
          />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
              <div className="d-flex justify-content-between align-items-center address">
                <div>
                  <p>
                    Brunch Co. Gelen
                    <br /> Shariah, United Arab Emirates
                    <br /> Saif Office (Q1-07-052/A)
                  </p>
                </div>
                <div>
                  +971 58 175 5868
                  <br /> info@gelen-is.com
                </div>
              </div>
            </Nav>
            <Nav className="d-flex justify-content-between align-items-center">
              <Nav.Link href="#deets">
                <Form.Control
                  type="search"
                  placeholder="Equipment or attachements? "
                  className="me-2 serach-input"
                  aria-label="Search"
                  onChange={(e) => {
                    setTimeout(
                      () => dispatch(filterSearch(e.target.value)),
                      1000
                    );
                  }}
                />
                <div className="search-ico">
                  <img src="/assets/images/camera.png" />
                </div>
              </Nav.Link>
              <Nav.Link href="#" onClick={onOpenModal}>
                <img src="/assets/images/Vector.png" />{" "}
                <span style={{ color: "#F3D112" }}>{wishData.length}</span>
              </Nav.Link>
            </Nav>
            <Nav.Link href="">
              <img
                clasName=""
                src="/assets/images/Menu.png"
                onClick={() => setDrawerOpen(!draweropen)}
              />
            </Nav.Link>
          </Navbar.Collapse>
        </Navbar>
      </div>
      <div className="mobileview-menu">
        <div className="d-flex ">
          <img
            src="/assets/images/slider.png"
            height={30}
            onClick={() => setFilter(true)}
          />
          <p>
            <img
              src="/assets/images/Vector.png"
              width={30}
              onClick={onOpenModal}
            />
            &nbsp;
            <span className="number">{wishData.length}</span>
          </p>
        </div>
      </div>
      <Modal
        show={open}
        onHide={onCloseModal}
        aria-labelledby="ModalHeader"
        centered
        size="lg"
        styles={{
          width: wishData.length == 0 ? "930px" : "auto",
          animationFillMode: "forwards",
          borderRadius: 20,
          padding: "1rem",
        }}
        closeButton
      >
        <div style={{ margin: 10 }}>
          <Modal.Header closeButton style={{ marginBottom: 10 }}>
            <Modal.Title closeButton>Wishlist</Modal.Title>
          </Modal.Header>
          <div className="">
            <Container className="catalog-details catalog-modal ">
              <div className="wishlist-products">
                <h5>Added Items</h5>
                {wishData.length > 0
                  ? wishData.map((wish) => (
                      <Card
                        style={{
                          marginTop: 10,
                          boxShadow:
                            "rgb(190, 190, 190) 0px 2px 11px, rgb(255, 255, 255) 4px -6px 25px",
                          border: "none",
                          borderRadius: 20,
                          // position: "",
                        }}
                      >
                        <Card.Body>
                          <div
                            className={`d-flex justify-content-between ${
                              width < 575
                                ? "card-content-mobile"
                                : "card-content"
                            }`}
                          >
                            <div>
                              <Card.Img
                                style={{ width: "" }}
                                variant="top"
                                src="/assets/images/new.png"
                              />
                            </div>
                            <div className="description">
                              <Card.Title className="mb-4">
                                {wish.Title}
                              </Card.Title>
                              <p>Working Hours: 5,883</p>
                              <p>Operating Weight: 6,000 Kg</p>
                              <p>Bucket Capacity: 1.1 m3</p>
                              <p>Engine Power:42 kW @ 2400 rpm</p>
                            </div>
                            <div className="d-flex flex-column justify-content-between align-items-center p-2">
                              <div className="icon-links">
                                <ul>
                                  <li>
                                    <img
                                      src="/assets/images/share.png"
                                      className="img-button"
                                      onClick={onOpenModal2}
                                    />
                                  </li>
                                  <li>
                                    <img
                                      src="/assets/images/plus.png"
                                      className="img-button"
                                      onClick={() => {
                                        SetCompareCookies({
                                          id: wish.id,
                                          ...wish.attributes,
                                        });
                                        dispatch(getCompareList());
                                      }}
                                    />
                                  </li>{" "}
                                  {/* <li>
                                  <img
                                    src={
                                      CheckWishListProduct(wish.id) == true
                                        ? "/assets/images/wishlistadd.png"
                                        : "/assets/images/add.png"
                                    }
                                    className="img-button"
                                    width={
                                      CheckWishListProduct(wish.id)
                                        ? 40
                                        : "auto"
                                    }
                                    onClick={() => {
                                      SetWishCookies({
                                        id: wish.id,
                                        ...wish.attributes,
                                      });
                                      dispatch(getWishList());
                                    }}
                                  />
                                </li> */}
                                </ul>
                              </div>
                              <div>
                                <p className="text-center upperText">
                                  get
                                  <br /> the best <br />
                                  price!
                                </p>
                              </div>
                              <div>
                                {/* <Button className="call-btn">
                                <img
                                  className="me-2"
                                  src="/assets/images/call.png"
                                />
                                Call Us
                              </Button> */}
                                <a
                                  href="tel:+971581755868"
                                  className="call-btn"
                                >
                                  <img
                                    className="me-2"
                                    src="/assets/images/call.png"
                                  />
                                  Call Us
                                </a>
                              </div>
                            </div>
                          </div>
                        </Card.Body>
                        <div
                          style={
                            width >= 1250
                              ? {
                                  position: "absolute",
                                  top: "0%",
                                  right: "-26px",
                                  cursor: "pointer",
                                }
                              : width >= 2250
                              ? {
                                  position: "absolute",
                                  top: "0%",
                                  right: "-26px",
                                  cursor: "pointer",
                                }
                              : {
                                  position: "absolute",
                                  top: "0%",
                                  right: "-26px",
                                  cursor: "pointer",
                                }
                          }
                        >
                          <img
                            src="/assets/images/cross.png"
                            height={20}
                            onClick={() => {
                              RemoveWishCookie(wish);
                              dispatch(getWishList());
                            }}
                          />
                        </div>
                      </Card>
                    ))
                  : "No Item Added to Wish List"}
              </div>
              <div className="d-flex justify-content-end mt-4">
                <Button className="shop-btn" onClick={() => onCloseModal(true)}>
                  Continue Shopping
                </Button>
              </div>
            </Container>
          </div>
        </div>
      </Modal>
      <div style={{ position: "relative" }}>
        <Drawer
          open={draweropen}
          direction="right"
          style={{
            background: "#000",
            padding: "177px 0 177px 40px",
            width: "390px",
          }}
        >
          <div
            className="drawmenu-close"
            onClick={() => setDrawerOpen(!draweropen)}
          ></div>
          <Nav.Link className="drawmenu-tab" href="#">
            News
          </Nav.Link>
          <Nav.Link className="drawmenu-tab" href="#">
            FAQ
          </Nav.Link>
          <Nav.Link className="drawmenu-tab" href="#">
            Services
          </Nav.Link>
          <Nav.Link className="drawmenu-tab" href="#">
            About
          </Nav.Link>
          <Nav.Link className="drawmenu-tab" href="#">
            Home
          </Nav.Link>
          <Nav.Link className="drawmenu-tab" href="#">
            Contact Us
          </Nav.Link>
          <Nav.Link className="drawmenu-tab drawmenu-tab-yellow" href="#">
            <img src="/assets/images/Vector.png" />
            Wishlist
          </Nav.Link>
        </Drawer>
      </div>
    </>
  );
};
export default CatalogHeader;
