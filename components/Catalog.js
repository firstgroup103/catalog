/* eslint-disable react/jsx-key */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */
import CatalogHeader from "./common/catalogheader";
import { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import "react-responsive-modal/styles.css";
// import { Modal } from "react-responsive-modal";
import {
  Container,
  Row,
  Col,
  Tabs,
  Tab,
  Accordion,
  Form,
  ProgressBar,
  Breadcrumb,
  Button,
  Card,
  Table,
  Modal,
} from "react-bootstrap";
import {
  EmailShareButton,
  FacebookIcon,
  FacebookShareButton,
  TwitterIcon,
  TwitterShareButton,
  WhatsappIcon,
  WhatsappShareButton,
  EmailIcon,
} from "react-share";
import Link from "next/link";
import {
  getdata,
  filterdata,
  share,
  getCompareList,
  filterRecentdata,
  getWishList,
  filterRecent,
  setHasMore,
  setQueryParameter,
} from "../libs/store/slice";
// import component 👇
import Drawer from "react-modern-drawer";

//import styles 👇
import "react-modern-drawer/dist/index.css";
import { useDispatch, useSelector } from "react-redux";
import { wishCountHandler } from "../libs/api";
import { useWindowSize } from "../src/utils/usewindowsize";
// import "react-rangeslider/lib/index.css";
import {
  SetCompareCookies,
  SetWishCookies,
  ReturnYears,
  RemoveCompareCookie,
  ASSETBASE,
  RemoveWishCookie,
} from "@/utils/common";
import Cookies from "js-cookie";
import { toast } from "react-toastify";
import Loader from "./common/CarLoader";
import Footer from "./common/footer";
import unstyledSlider from "@mui/material/Slider";

import { styled } from "@mui/material/styles";

const Slider = styled(unstyledSlider)`
  color: #132528;

  margin-bottom: 20px;

  & .MuiSlider-root {
    height: 1px;
  }

  & .MuiSlider-rail {
    height: 1px;
  }

  & .MuiSlider-track {
    height: 1px;
  }

  & .MuiSlider-thumb {
    background: #f3f3f3;
    border: 1px solid #132528;
  }

  & .MuiSlider-valueLabel {
    transform: translateY(100%) scale(1);
    background: transparent;
    color: #132528;
  }

  & .MuiSlider-valueLabelOpen {
    transform: translateY(100%) scale(1) !important;
    background: transparent;
    color: #132528;
  }

  & .MuiSlider-valueLabel::hover {
    transform: translateY(100%) scale(1);
    background: transparent;
    color: #132528;
  }

  & .MuiSlider-valueLabel::before {
    display: none;
  }
`;

const Catalog = () => {
  const [sliderMileage, setSliderMileage] = useState([0, 990000]);

  const handleChangeSliderMileage = (event, newValue) => {
    setSliderMileage(newValue);
    setMileageFilter(newValue);
  };

  const [sliderHours, setSliderHours] = useState([0, 90000]);

  const handleChangeSliderHours = (event, newValue) => {
    setSliderHours(newValue);
    setHoursFilter(newValue);
  };

  const [sliderWeight, setSliderWeight] = useState([0, 220000]);

  const handleChangeSliderWeight = (event, newValue) => {
    setSliderWeight(newValue);
    setWeightFilter(newValue);
  };

  const dispatch = useDispatch();
  const { cataloglist, wishData, compareData, tag, hasmore, ...restState } =
    useSelector((state) => state.catalog);
  console.log(cataloglist, "REDUX");
  const [pageSize, setPageSize] = useState(8);
  const [page, setPage] = useState(1);
  const [industryFilter, setIndustryFilter] = useState([]);
  const [EquipmentFilter, setEquipmentFilter] = useState([]);
  const [SubEquipmentFilter, setSubEquipmentFilter] = useState([]);
  const [yearFilter, setYearFilter] = useState([]);
  const [saleTypeFilter, setsaleTypeFilter] = useState([]);
  // const [StatusFilter, setStatusFilter] = useState(["Rent"]);
  const [StatusFilter, setStatusFilter] = useState(["Rent", "Buy"]);
  const [ManYearFrom, setManYearFrom] = useState("");
  const [WeightFrom, setWeightFrom] = useState("");
  const [WeightFilter, setWeightFilter] = useState([]);
  const [HoursFrom, setHoursFrom] = useState("");
  const [HoursFilter, setHoursFilter] = useState([]);
  const [MileageFrom, setMileageFrom] = useState("");
  const [MileageFilter, setMileageFilter] = useState([]);
  const [BrandFilter, setBrandFilter] = useState([]);
  const [tagFilter, setTagFilter] = useState([]);

  const [shareObj, setshareurl] = useState({});
  const [Date, setDate] = useState([]);
  const [years, setYears] = useState([]);
  const IndustryArray = [];
  const EquipmentArray = [];
  const SubEquipmentArray = [];
  const BrandArray = [];
  const YearArray = [];
  const weightArray = [];
  const hoursArray = [];

  const MileageArray = [];
  const [getFillers, setFillers] = useState("all");
  const [counter, setCounter] = useState(0);
  const [activetab, setActiveTab] = useState("all");
  const [open, setOpen] = useState(false);
  const [open2, setOpen2] = useState(false);

  const onOpenModal = () => setOpen(true);
  const onOpenModal2 = () => setOpen2(true);

  const onCloseModal = () => setOpen(false);
  const onCloseModal2 = () => setOpen2(false);

  const CheckWishListProduct = (id) => {
    const Data = wishData.find((da) => da.id == id);
    // console.log(Data, "dadad");
    if (Data !== undefined && Object.keys(Data).length > 0) {
      return true;
    } else {
      false;
    }
  };
  const [compareProduct, SetCompareProducts] = useState([]);
  async function UpdateData(data, id) {
    console.log(data, "hi vinay");

    if (!counter) {
      const increment = await dispatch(
        share({ wishListCount: data?.wishListCount + 1, id })
      );
      setCounter(counter);
      return increment;
    } else {
      const decrement = await dispatch(
        share({ wishListCount: data?.wishListCount - 1, id })
      );
      setCounter(counter);
      return decrement;
    }
  }

  const getdataFilters = {
      industryFilter,
      EquipmentFilter,
      SubEquipmentFilter,
      BrandFilter,
      yearFilter,
      saleTypeFilter,
      StatusFilter,
      tagFilter,
      WeightFilter,
      HoursFilter,
      MileageFilter,
      page : 1,
      pageSize : 2,
  }

  useEffect(() => {
    async function FetchData() {
      const res = await dispatch(
        getdata(getdataFilters)
      );

      console.log(res, "useEffectRes");

      // console.log(res.payload.data, "All");
      const min = 1952;
      setYears(ReturnYears());
      console.log("Cookies.get()");
      if (Cookies.get("compare") !== undefined) {
        console.log(Cookies.get("compare"), "Cookies.get()");
        dispatch(getCompareList());
      }
    }

    FetchData();

    // UpdateData();
  }, [
    dispatch,
    industryFilter,
    EquipmentFilter,
    SubEquipmentFilter,
    BrandFilter,
    yearFilter,
    saleTypeFilter,
    StatusFilter,
    tagFilter,
    WeightFilter,
    HoursFilter,
    MileageFilter,
    page,
    sliderHours,
    sliderMileage,
    sliderWeight,
    pageSize,
  ]);

  
  const size = useWindowSize();
  const [width, setWidth] = useState(null);
  const [filter, setFilter] = useState(false);
  console.log(size && size?.width > 575, size?.width, "ggggg");
  useEffect(() => {
    async function setscreenWidth() {
      setWidth(size?.width);
    }
    setscreenWidth();
  }, [size.width]);
  console.log(WeightFilter, "industryfilter");
  const LoaderFunction = () => {
    if (hasmore == true) {
      return <Loader />;
    }
  };
  const resetFilters = () => {
    const check = document.getElementsByTagName("input");
    for (var i = 0; i < check.length; i++) {
      if (check[i].type == "checkbox") {
        check[i].checked = false;
      }
    }
  };
  return (
    <>
      <div className="catalog">
        <CatalogHeader setFilter={setFilter} onOpenModal2={onOpenModal2} />
        <Container fluid className="catalog-wrapper">
          <Row>
            {width < 575 && (
              <Col sm={3} md={3} className={"bgGrey"}>
                <div className="buton-filter">
                  <Button
                    className=" filter-btn"
                    onClick={() => setFilter(!filter)}
                  >
                    Filter
                  </Button>
                </div>
                <Drawer
                  open={filter}
                  onClose={() => setFilter(false)}
                  direction="right"
                  className="bla bla bla"
                >
                  <div className="sidebar">
                    <Tabs
                      defaultActiveKey="Rent"
                      id="uncontrolled-tab-example"
                      className="mb-3 pt-4 "
                      onSelect={(e) => {
                        console.log(e, "ererere");
                        const tab =
                          e == "Buy" ? "Buy" : e == "Rent" ? "Rent" : "";
                        const arr = [];
                        arr.push(tab);
                        setStatusFilter(tab == "" ? [] : arr);
                      }}
                    >
                      <Tab eventKey="Rent" title="Rent"></Tab>
                      <Tab eventKey="Buy" title="Buy"></Tab>
                    </Tabs>
                    <div className="tags d-flex justify-content-between">
                      <div
                        className={`${tag == "New" ? "tag-active" : "tag"}`}
                        onClick={() => {
                          const arr = [];
                          arr.push("New");
                          setTagFilter(arr);
                          dispatch(filterdata("New"));
                        }}
                      >
                        New
                      </div>
                      <div
                        className={`${
                          tag == "Like New" ? "tag-active" : "tag"
                        }`}
                        onClick={() => {
                          const arr = [];
                          arr.push("Like New");
                          setTagFilter(arr);
                          dispatch(filterdata("Like New"));
                        }}
                      >
                        Like New
                      </div>
                      <div
                        className={` ${tag == "Used" ? "tag-active" : "tag"}`}
                        onClick={() => {
                          const arr = [];
                          arr.push("Used");
                          setTagFilter(arr);
                          dispatch(filterdata("Used"));
                        }}
                      >
                        Used
                      </div>
                    </div>
                    <div className="filter mt-4 d-flex justify-content-between align-items-center border-bottom">
                      <h4>
                        <img src="/assets/images/filter.png" />
                        Filters
                      </h4>
                      <div
                        className="clear-text"
                        onClick={() => {
                          setEquipmentFilter([]);
                          setBrandFilter([]);
                          setHoursFilter([]);
                          setIndustryFilter([]);
                          resetFilters();
                          setSliderHours([0, 90000]);
                          setSliderMileage([0, 990000]);
                          setSliderWeight([0, 200000]);
                        }}
                      >
                        Clear all
                      </div>
                    </div>
                    <Accordion className="top-accordion" defaultValue={false}>
                      <Accordion.Item eventKey="0">
                        <Accordion.Header>Industries</Accordion.Header>
                        <Accordion.Body>
                          <Form.Check
                            label="Construction"
                            id="Construction"
                            onChange={(e) => {
                              if (e.target.checked) {
                                IndustryArray.push("Construction");
                                const data = [
                                  ...industryFilter,
                                  ...IndustryArray,
                                ];
                                setIndustryFilter(data);
                              } else {
                                IndustryArray.pop("Quarry-Aggregate");
                                const data = [...IndustryArray];
                                setIndustryFilter(data);
                              }
                            }}
                          />
                          <Form.Check
                            label="Quarry & Aggregate"
                            id="Quarry & Aggregate"
                            onChange={(e) => {
                              if (e.target.checked) {
                                IndustryArray.push("Quarry-Aggregate");
                                const data = [
                                  ...industryFilter,
                                  ...IndustryArray,
                                ];
                                setIndustryFilter(data);
                              } else {
                                IndustryArray.pop("Quarry-Aggregate");
                                const data = [...IndustryArray];
                                setIndustryFilter(data);
                              }
                            }}
                          />
                          <Form.Check
                            label="Mining"
                            id="Mining"
                            onChange={(e) => {
                              if (e.target.checked) {
                                IndustryArray.push("Mining");
                                const data = [
                                  ...industryFilter,
                                  ...IndustryArray,
                                ];
                                setIndustryFilter(data);
                              } else {
                                IndustryArray.pop("Mining");
                                const data = [...IndustryArray];
                                setIndustryFilter(data);
                              }
                            }}
                          />
                          <Form.Check
                            label="Oil & Gas"
                            id="Oil & Gas"
                            onChange={(e) => {
                              if (e.target.checked) {
                                IndustryArray.push("Oil-Gas");
                                const data = [
                                  ...industryFilter,
                                  ...IndustryArray,
                                ];
                                setIndustryFilter(data);
                              } else {
                                IndustryArray.pop("Oil-Gas");
                                const data = [...IndustryArray];
                                setIndustryFilter(data);
                              }
                            }}
                          />
                        </Accordion.Body>
                      </Accordion.Item>
                      <Accordion.Item eventKey="1">
                        <Accordion.Header>Equipment Type</Accordion.Header>
                        <Accordion.Body>
                          <Form.Check
                            label="Bulldozers"
                            id="Bulldozers"
                            onChange={(e) => {
                              if (e.target.checked) {
                                EquipmentArray.push("Bulldozers");
                                const data = [
                                  ...EquipmentFilter,
                                  ...EquipmentArray,
                                ];
                                setEquipmentFilter(data);
                              } else {
                                EquipmentArray.pop("Bulldozers");
                                const data = [...EquipmentArray];
                                setEquipmentFilter(data);
                              }
                            }}
                          />
                          <Form.Check
                            label="Crawler Dozers"
                            id="Crawler Dozers"
                            onChange={(e) => {
                              if (e.target.checked) {
                                SubEquipmentArray.push("Crawler-Dozers");
                                const data = [
                                  ...SubEquipmentFilter,
                                  ...SubEquipmentArray,
                                ];
                                setSubEquipmentFilter(data);
                              } else {
                                SubEquipmentArray.pop("Crawler-Dozers");
                                const data = [...SubEquipmentArray];
                                setSubEquipmentFilter(data);
                              }
                            }}
                          />
                          <Form.Check
                            label="Mini Excavators"
                            id="Mini Excavators"
                            onChange={(e) => {
                              if (e.target.checked) {
                                SubEquipmentArray.push("Mini-Excavators");
                                const data = [
                                  ...SubEquipmentFilter,
                                  ...SubEquipmentArray,
                                ];
                                setSubEquipmentFilter(data);
                              } else {
                                SubEquipmentArray.pop("Mini-Excavators");
                                const data = [...SubEquipmentArray];
                                setSubEquipmentFilter(data);
                              }
                            }}
                          />
                          <Form.Check
                            label="Off-Road Trucks"
                            id="Off-Road Trucks"
                            onChange={(e) => {
                              if (e.target.checked) {
                                EquipmentArray.push("Off-Road-Trucks");
                                const data = [
                                  ...EquipmentFilter,
                                  ...EquipmentArray,
                                ];
                                setEquipmentFilter(data);
                              } else {
                                EquipmentArray.pop("Off-Road-Trucks");
                                const data = [...EquipmentArray];
                                setEquipmentFilter(data);
                              }
                            }}
                          />
                          <Form.Check
                            label="Excavators"
                            id="Excavators"
                            onChange={(e) => {
                              if (e.target.checked) {
                                EquipmentArray.push("Excavators");
                                const data = [
                                  ...EquipmentFilter,
                                  ...EquipmentArray,
                                ];
                                setEquipmentFilter(data);
                              } else {
                                EquipmentArray.pop("Excavators");
                                const data = [...EquipmentArray];
                                setEquipmentFilter(data);
                              }
                            }}
                          />
                        </Accordion.Body>
                      </Accordion.Item>

                      <Accordion.Item eventKey="2">
                        <Accordion.Header>Brand</Accordion.Header>
                        <Accordion.Body>
                          <Form.Check
                            label="Kobelco"
                            id="Kobelco"
                            onChange={(e) => {
                              if (e.target.checked) {
                                BrandArray.push("Kobelco");
                                const data = [...BrandFilter, ...BrandArray];
                                setBrandFilter(data);
                              } else {
                                BrandArray.pop("Kobelco");
                                const data = [...BrandArray];
                                setBrandFilter(data);
                              }
                            }}
                          />
                          <Form.Check
                            label="Dynapac"
                            id="Dynapac"
                            onChange={(e) => {
                              if (e.target.checked) {
                                BrandArray.push("Dynapac");
                                const data = [...BrandFilter, ...BrandArray];
                                setBrandFilter(data);
                              } else {
                                BrandArray.pop("Dynapac");
                                const data = [...BrandArray];
                                setBrandFilter(data);
                              }
                            }}
                          />
                          <Form.Check
                            label="Sakai"
                            id="Sakai"
                            onChange={(e) => {
                              if (e.target.checked) {
                                BrandArray.push("Sakai");
                                const data = [...BrandFilter, ...BrandArray];
                                setBrandFilter(data);
                              } else {
                                BrandArray.pop("Sakai");
                                const data = [...BrandArray];
                                setBrandFilter(data);
                              }
                            }}
                          />
                          <Form.Check
                            label="Cat"
                            id="Cat"
                            onChange={(e) => {
                              if (e.target.checked) {
                                BrandArray.push("Cat");
                                const data = [...BrandFilter, ...BrandArray];
                                setBrandFilter(data);
                              } else {
                                BrandArray.pop("Cat");
                                const data = [...BrandArray];
                                setBrandFilter(data);
                              }
                            }}
                          />
                          <Form.Check
                            label="Komatsu"
                            id="Komatsu"
                            onChange={(e) => {
                              if (e.target.checked) {
                                BrandArray.push("Komatsu");
                                const data = [...BrandFilter, ...BrandArray];
                                setBrandFilter(data);
                              } else {
                                BrandArray.pop("Komatsu");
                                const data = [...BrandArray];
                                setBrandFilter(data);
                              }
                            }}
                          />
                          <Form.Check
                            label="JCB"
                            id="JCB"
                            onChange={(e) => {
                              if (e.target.checked) {
                                BrandArray.push("JCB");
                                const data = [...BrandFilter, ...BrandArray];
                                setBrandFilter(data);
                              } else {
                                BrandArray.pop("JCB");
                                const data = [...BrandArray];
                                setBrandFilter(data);
                              }
                            }}
                          />
                        </Accordion.Body>
                      </Accordion.Item>

                      <Accordion.Item eventKey="4">
                        <Accordion.Header>Year of manufacture</Accordion.Header>
                        <Accordion.Body>
                          <div className="d-flex justify-content-between">
                            <div>
                              <label>From</label>
                              <Form.Select
                                size="sm"
                                onChange={(e) => {
                                  setManYearFrom(e.target.value);
                                }}
                              >
                                {years.map((yr) => (
                                  <option value={yr}>{yr}</option>
                                ))}
                              </Form.Select>
                            </div>
                            <div>
                              <label>To</label>
                              <Form.Select
                                size="sm"
                                onChange={(e) => {
                                  YearArray.push(ManYearFrom);
                                  YearArray.push(e.target.value);
                                  setYearFilter(YearArray);
                                  console.log(YearArray, "data");
                                }}
                              >
                                {years.map((yr) => (
                                  <option value={yr} selected>
                                    {yr}
                                  </option>
                                ))}
                              </Form.Select>
                            </div>
                          </div>
                        </Accordion.Body>
                      </Accordion.Item>
                      <Accordion.Item eventKey="5">
                        <Accordion.Header>
                          Operating Weight (kg)
                        </Accordion.Header>
                        <Accordion.Body>
                          <div className="d-flex justify-content-between">
                            <Slider
                              getAriaLabel={() => "Operating Weight (kg)"}
                              value={sliderWeight}
                              onChange={handleChangeSliderWeight}
                              valueLabelDisplay="auto"
                              max={"220000"}
                            />
                          </div>
                        </Accordion.Body>
                      </Accordion.Item>
                      <Accordion.Item eventKey="6">
                        <Accordion.Header>Working hours</Accordion.Header>
                        <Accordion.Body>
                          <div className="d-flex justify-content-between">
                            <Slider
                              getAriaLabel={() => "Working hours"}
                              value={sliderHours}
                              onChange={handleChangeSliderHours}
                              valueLabelDisplay="auto"
                              max={"90000"}
                            />
                          </div>
                        </Accordion.Body>
                      </Accordion.Item>
                      <Accordion.Item eventKey="7">
                        <Accordion.Header>Mileage</Accordion.Header>
                        <Accordion.Body>
                          <div className="d-flex justify-content-between">
                            <Slider
                              getAriaLabel={() => "Mileage"}
                              value={sliderMileage}
                              onChange={handleChangeSliderMileage}
                              valueLabelDisplay="auto"
                              max={"990000"}
                            />
                          </div>
                        </Accordion.Body>
                      </Accordion.Item>
                    </Accordion>
                  </div>
                </Drawer>
              </Col>
            )}

            <Col
              sm={3}
              md={3}
              lg={3}
              xl={3}
              className={
                width < 575 || width > 768
                  ? "mobile-sidebar"
                  : // : width < 889
                    // ? "mobile-sidebar"
                    "bgGrey "
              }
            >
              <div className="sidebar">
                <Tabs
                  defaultActiveKey="Rent"
                  id="uncontrolled-tab-example"
                  className="mb-3 pt-4 "
                  onSelect={(e) => {
                    console.log(e, "ererere");
                    const tab = e == "Buy" ? "Buy" : e == "Rent" ? "Rent" : "";
                    const arr = [];
                    arr.push(tab);
                    setStatusFilter(tab == "" ? [] : arr);
                  }}
                >
                  <Tab eventKey="Rent" title="Rent"></Tab>
                  <Tab eventKey="Buy" title="Buy"></Tab>
                </Tabs>
                <div className="tags d-flex justify-content-between">
                  <div
                    className={`${tag == "New" ? "tag-active" : "tag"}`}
                    onClick={() => {
                      const arr = [];
                      arr.push("New");
                      setTagFilter(arr);
                      dispatch(filterdata("New"));
                    }}
                  >
                    New
                  </div>
                  <div
                    className={`${tag == "Like New" ? "tag-active" : "tag"}`}
                    onClick={() => {
                      const arr = [];
                      arr.push("Like New");
                      setTagFilter(arr);
                      dispatch(filterdata("Like New"));
                    }}
                  >
                    Like New
                  </div>
                  <div
                    className={` ${tag == "Used" ? "tag-active" : "tag"}`}
                    onClick={() => {
                      const arr = [];
                      arr.push("Used");
                      setTagFilter(arr);
                      dispatch(filterdata("Used"));
                    }}
                  >
                    Used
                  </div>
                </div>
                <div className="filter mt-4 d-flex justify-content-between align-items-center border-bottom">
                  <h4>
                    <img src="/assets/images/filter.png" />
                    Filters
                  </h4>
                  <div
                    className="clear-text"
                    onClick={() => {
                      setEquipmentFilter([]);
                      setBrandFilter([]);
                      setWeightFilter([]);
                      setMileageFilter([]);
                      setHoursFilter([]);
                      setIndustryFilter([]);
                      resetFilters();
                      setSliderHours([0, 90000]);
                      setSliderMileage([0, 990000]);
                      setSliderWeight([0, 200000]);
                    }}
                  >
                    Clear all
                  </div>
                </div>
                <Accordion
                  className="top-accordion"
                  defaultValue={false}
                  alwaysOpen
                >
                  <Accordion.Item eventKey="0">
                    <Accordion.Header>Industries</Accordion.Header>
                    <Accordion.Body>
                      <Form.Check
                        label="Construction"
                        id="Construction"
                        onChange={(e) => {
                          if (e.target.checked) {
                            IndustryArray.push("Construction");
                            const data = [...industryFilter, ...IndustryArray];
                            setIndustryFilter(data);
                          } else {
                            IndustryArray.pop("Quarry-Aggregate");
                            const data = [...IndustryArray];
                            setIndustryFilter(data);
                          }
                        }}
                      />
                      <Form.Check
                        label="Quarry & Aggregate"
                        id="Quarry & Aggregate"
                        onChange={(e) => {
                          if (e.target.checked) {
                            IndustryArray.push("Quarry-Aggregate");
                            const data = [...industryFilter, ...IndustryArray];
                            setIndustryFilter(data);
                          } else {
                            IndustryArray.pop("Quarry-Aggregate");
                            const data = [...IndustryArray];
                            setIndustryFilter(data);
                          }
                        }}
                      />
                      <Form.Check
                        label="Mining"
                        id="Mining"
                        onChange={(e) => {
                          if (e.target.checked) {
                            IndustryArray.push("Mining");
                            const data = [...industryFilter, ...IndustryArray];
                            setIndustryFilter(data);
                          } else {
                            IndustryArray.pop("Mining");
                            const data = [...IndustryArray];
                            setIndustryFilter(data);
                          }
                        }}
                      />
                      <Form.Check
                        label="Oil & Gas"
                        id="Oil & Gas"
                        onChange={(e) => {
                          if (e.target.checked) {
                            IndustryArray.push("Oil-Gas");
                            const data = [...industryFilter, ...IndustryArray];
                            setIndustryFilter(data);
                          } else {
                            IndustryArray.pop("Oil-Gas");
                            const data = [...IndustryArray];
                            setIndustryFilter(data);
                          }
                        }}
                      />
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="1">
                    <Accordion.Header>Equipment Type</Accordion.Header>
                    <Accordion.Body>
                      <Form.Check
                        label="Bulldozers"
                        id="Bulldozers"
                        onChange={(e) => {
                          if (e.target.checked) {
                            EquipmentArray.push("Bulldozers");
                            const data = [
                              ...EquipmentFilter,
                              ...EquipmentArray,
                            ];
                            setEquipmentFilter(data);
                          } else {
                            EquipmentArray.pop("Bulldozers");
                            const data = [...EquipmentArray];
                            setEquipmentFilter(data);
                          }
                        }}
                      />
                      <Form.Check
                        label="Crawler Dozers"
                        id="Crawler Dozers"
                        onChange={(e) => {
                          if (e.target.checked) {
                            SubEquipmentArray.push("Crawler-Dozers");
                            const data = [
                              ...SubEquipmentFilter,
                              ...SubEquipmentArray,
                            ];
                            setSubEquipmentFilter(data);
                          } else {
                            SubEquipmentArray.pop("Crawler-Dozers");
                            const data = [...SubEquipmentArray];
                            setSubEquipmentFilter(data);
                          }
                        }}
                      />
                      <Form.Check
                        label="Mini Excavators"
                        id="Mini Excavators"
                        onChange={(e) => {
                          if (e.target.checked) {
                            SubEquipmentArray.push("Mini-Excavators");
                            const data = [
                              ...SubEquipmentFilter,
                              ...SubEquipmentArray,
                            ];
                            setSubEquipmentFilter(data);
                          } else {
                            SubEquipmentArray.pop("Mini-Excavators");
                            const data = [...SubEquipmentArray];
                            setSubEquipmentFilter(data);
                          }
                        }}
                      />
                      <Form.Check
                        label="Off-Road Trucks"
                        id="Off-Road Trucks"
                        onChange={(e) => {
                          if (e.target.checked) {
                            EquipmentArray.push("Off-Road-Trucks");
                            const data = [
                              ...EquipmentFilter,
                              ...EquipmentArray,
                            ];
                            setEquipmentFilter(data);
                          } else {
                            EquipmentArray.pop("Off-Road-Trucks");
                            const data = [...EquipmentArray];
                            setEquipmentFilter(data);
                          }
                        }}
                      />
                      <Form.Check
                        label="Excavators"
                        id="Excavators"
                        onChange={(e) => {
                          if (e.target.checked) {
                            EquipmentArray.push("Excavators");
                            const data = [
                              ...EquipmentFilter,
                              ...EquipmentArray,
                            ];
                            setEquipmentFilter(data);
                          } else {
                            EquipmentArray.pop("Excavators");
                            const data = [...EquipmentArray];
                            setEquipmentFilter(data);
                          }
                        }}
                      />
                    </Accordion.Body>
                  </Accordion.Item>

                  <Accordion.Item eventKey="2">
                    <Accordion.Header>Brand</Accordion.Header>
                    <Accordion.Body>
                      <Form.Check
                        label="Kobelco"
                        id="Kobelco"
                        onChange={(e) => {
                          if (e.target.checked) {
                            BrandArray.push("Kobelco");
                            const data = [...BrandFilter, ...BrandArray];
                            setBrandFilter(data);
                          } else {
                            BrandArray.pop("Kobelco");
                            const data = [...BrandArray];
                            setBrandFilter(data);
                          }
                        }}
                      />
                      <Form.Check
                        label="Dynapac"
                        id="Dynapac"
                        onChange={(e) => {
                          if (e.target.checked) {
                            BrandArray.push("Dynapac");
                            const data = [...BrandFilter, ...BrandArray];
                            setBrandFilter(data);
                          } else {
                            BrandArray.pop("Dynapac");
                            const data = [...BrandArray];
                            setBrandFilter(data);
                          }
                        }}
                      />
                      <Form.Check
                        label="Sakai"
                        id="Sakai"
                        onChange={(e) => {
                          if (e.target.checked) {
                            BrandArray.push("Sakai");
                            const data = [...BrandFilter, ...BrandArray];
                            setBrandFilter(data);
                          } else {
                            BrandArray.pop("Sakai");
                            const data = [...BrandArray];
                            setBrandFilter(data);
                          }
                        }}
                      />
                      <Form.Check
                        label="Cat"
                        id="Cat"
                        onChange={(e) => {
                          if (e.target.checked) {
                            BrandArray.push("Cat");
                            const data = [...BrandFilter, ...BrandArray];
                            setBrandFilter(data);
                          } else {
                            BrandArray.pop("Cat");
                            const data = [...BrandArray];
                            setBrandFilter(data);
                          }
                        }}
                      />
                      <Form.Check
                        label="Komatsu"
                        id="Komatsu"
                        onChange={(e) => {
                          if (e.target.checked) {
                            BrandArray.push("Komatsu");
                            const data = [...BrandFilter, ...BrandArray];
                            setBrandFilter(data);
                          } else {
                            BrandArray.pop("Komatsu");
                            const data = [...BrandArray];
                            setBrandFilter(data);
                          }
                        }}
                      />
                      <Form.Check
                        label="JCB"
                        id="JCB"
                        onChange={(e) => {
                          if (e.target.checked) {
                            BrandArray.push("JCB");
                            const data = [...BrandFilter, ...BrandArray];
                            setBrandFilter(data);
                          } else {
                            BrandArray.pop("JCB");
                            const data = [...BrandArray];
                            setBrandFilter(data);
                          }
                        }}
                      />
                    </Accordion.Body>
                  </Accordion.Item>

                  <Accordion.Item eventKey="4">
                    <Accordion.Header>Year of manufacture</Accordion.Header>
                    <Accordion.Body>
                      <div className="d-flex justify-content-between">
                        <div>
                          <label>From</label>
                          <Form.Select
                            size="sm"
                            onChange={(e) => {
                              setManYearFrom(e.target.value);
                            }}
                          >
                            {years.map((yr) => (
                              <option value={yr}>{yr}</option>
                            ))}
                          </Form.Select>
                        </div>
                        <div>
                          <label>To</label>
                          <Form.Select
                            size="sm"
                            onChange={(e) => {
                              YearArray.push(ManYearFrom);
                              YearArray.push(e.target.value);
                              setYearFilter(YearArray);
                              console.log(YearArray, "data");
                            }}
                          >
                            {years.map((yr) => (
                              <option value={yr} selected>
                                {yr}
                              </option>
                            ))}
                          </Form.Select>
                        </div>
                      </div>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="5">
                    <Accordion.Header>Operating Weight (kg)</Accordion.Header>
                    <Accordion.Body>
                      <div className="d-flex justify-content-between">
                        <Slider
                          getAriaLabel={() => "Operating Weight (kg)"}
                          value={sliderWeight}
                          onChange={handleChangeSliderWeight}
                          valueLabelDisplay="on"
                          max={"220000"}
                        />
                      </div>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="6">
                    <Accordion.Header>Working hours</Accordion.Header>
                    <Accordion.Body>
                      <div className="d-flex justify-content-between">
                        <Slider
                          getAriaLabel={() => "Working hours"}
                          value={sliderHours}
                          onChange={handleChangeSliderHours}
                          valueLabelDisplay="auto"
                          max={"90000"}
                        />
                      </div>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="7">
                    <Accordion.Header>Mileage</Accordion.Header>
                    <Accordion.Body>
                      <div className="d-flex justify-content-between">
                        <Slider
                          getAriaLabel={() => "Mileage"}
                          value={sliderMileage}
                          onChange={handleChangeSliderMileage}
                          valueLabelDisplay="auto"
                          max={"990000"}
                        />
                      </div>
                    </Accordion.Body>
                  </Accordion.Item>
                </Accordion>
              </div>
            </Col>
            <Col sm={9} xl={9} md={width > 760 ? 12 : 9} lg={9}>
              <div className="equipment-sale p-4">
                <div className="d-flex justify-content-between align-items-center">
                  <div>
                    <h4>
                      <strong>Equipment</strong> for Sale
                    </h4>
                    <Breadcrumb>
                      <Breadcrumb.Item active href="#">
                        Home
                      </Breadcrumb.Item>
                      <Breadcrumb.Item href="">equipment</Breadcrumb.Item>
                    </Breadcrumb>
                  </div>
                  <div>
                    <Button
                      className="recent-btn"
                      onClick={(e) => {
                        const date = new window.Date(window.Date.now());
                        setTimeout(
                          () =>
                            dispatch(
                              filterRecent(date.toISOString().split("T", 1)[0])
                            ),
                          1000
                        );
                      }}
                    >
                      Recent{" "}
                      <img
                        style={{ marginLeft: "2rem" }}
                        src="/assets/images/arrow.png"
                      />
                    </Button>
                  </div>
                </div>
                <div className="equip-tabs">
                  <Tabs
                    defaultActiveKey={activetab}
                    onSelect={(e) => {
                      setActiveTab(e);
                      const tab =
                        e == "all"
                          ? ""
                          : e == "sale"
                          ? "For Sale"
                          : e == "reserve"
                          ? "Reserved"
                          : "Sold";
                      const arr = [];
                      arr.push(tab);
                      setsaleTypeFilter(tab == "" ? [] : arr);
                    }}
                    transition={false}
                    id="noanim-tab-example"
                    className="mb-3"
                  >
                    <Tab eventKey="all" title="All">
                      <Row>
                        <Col sm={width <= 898 ? 12 : 9}>
                          {tag !== "" && (
                            <div className="tags mb-4">
                              <div
                                className={
                                  tag == "Like New" ? "tagOther" : "tag"
                                }
                              >
                                {tag}{" "}
                                <img
                                  src="assets/images/kros.png"
                                  onClick={() => {
                                    dispatch(filterdata(""));
                                    setTagFilter([]);
                                    console.log(getdataFilters);
                                    dispatch(getdata(getdataFilters));
                                  }}
                                />
                              </div>
                            </div>
                          )}
                          {/* <div
                            id="scrollableDiv"
                            // style={{
                            //   height: "80%",
                            //   overflow: "auto",
                            //   display: "flex",
                            //   flexDirection: "column-reverse",
                            // }}
                          > */}
                          {/* <InfiniteScroll
                            dataLength={cataloglist && cataloglist?.length}
                            next={() => {
                              setPage(page + 1);
                              dispatch(setHasMore());
                              // setPageSize(pageSize + 6);
                            }}
                            style={{ height: "100%", overflowY: "scroll" }}
                            // style={{
                            //   display: "flex",
                            //   flexDirection: "column-reverse",
                            // }} //To put endMessage and loader to the top.
                            endMessage={
                              <p style={{ textAlign: "center" }}>
                                <b>Yay! You have seen it all</b>
                              </p>
                            }
                            // inverse={true} //
                            hasMore={true}
                            loader={() => LoaderFunction()}
                            // scrollableTarget="scrollableDiv"
                          > */}
                            {cataloglist &&
                              cataloglist?.length > 0 &&
                              cataloglist?.map((cat) => (
                                // eslint-disable-next-line react/jsx-key
                                <Card>
                                  <Card.Body>
                                    <div
                                      className={`d-flex justify-content-between ${
                                        width < 575 || width <= 898
                                          ? "card-content-mobile"
                                          : "card-content"
                                      }`}
                                    >
                                      <Link
                                        href={`/${cat.attributes.MachineSerial}/${cat.attributes.Slug}`}
                                        onClick={() => {
                                          dispatch(
                                            setQueryParameter({
                                              slug: cat.attributes.Slug,
                                              serialno:
                                                cat.attributes.MachineSerial,
                                            })
                                          );
                                        }}
                                        style={{
                                          display: "flex",
                                          justifyContent: "space-between",
                                        }}
                                      >
                                        <div>
                                          <Card.Img
                                            style={{ width: "240px" }}
                                            variant="top"
                                            src={
                                              cat?.attributes &&
                                              cat?.attributes?.profilePick &&
                                              cat?.attributes?.profilePick
                                                ?.data &&
                                              cat?.attributes?.profilePick
                                                ?.data[0]?.attributes.url
                                                ? `${ASSETBASE}${
                                                    cat?.attributes &&
                                                    cat?.attributes
                                                      ?.profilePick &&
                                                    cat?.attributes?.profilePick
                                                      ?.data &&
                                                    cat?.attributes?.profilePick
                                                      ?.data[0]?.attributes.url
                                                  }`
                                                : "/assets/images/pic2.png"
                                            }
                                          />
                                          <div className="tag-inner">
                                            <div className="tag1">
                                              {cat?.attributes?.SaleType}
                                            </div>
                                            <div className="tag2">
                                              {cat?.attributes?.Type}
                                            </div>
                                          </div>
                                        </div>
                                        <div className="description">
                                          <Card.Title className="mb-4">
                                            {cat?.attributes?.Title}
                                            <br />
                                          </Card.Title>
                                          <p>
                                            Working Hours:{" "}
                                            <strong style={{ fontSize: 12 }}>
                                              {" "}
                                              {cat?.attributes?.Workinghours}
                                            </strong>
                                          </p>
                                          <p>
                                            Operating Weight:{" "}
                                            <strong style={{ fontSize: 12 }}>
                                              {" "}
                                              {cat?.attributes?.Operatingweight}
                                            </strong>
                                          </p>
                                          <p>
                                            Bucket Capacity:
                                            <strong style={{ fontSize: 12 }}>
                                              {" "}
                                              {cat?.attributes?.bucketcapacity}
                                            </strong>
                                          </p>
                                          <p>
                                            Engine Power:
                                            <strong style={{ fontSize: 12 }}>
                                              {cat?.attributes?.EnginePower}
                                            </strong>
                                          </p>
                                        </div>
                                      </Link>
                                      <div className="d-flex flex-column justify-content-between align-items-center flex-icon">
                                        <div className="icon-links">
                                          <ul>
                                            <li>
                                              <img
                                                src="/assets/images/share.png"
                                                className="img-button"
                                                onClick={() => {
                                                  onOpenModal2();
                                                  setshareurl({
                                                    title: cat.attributes.Title,
                                                    image:
                                                      cat?.attributes &&
                                                      cat?.attributes
                                                        ?.profilePick &&
                                                      cat?.attributes
                                                        ?.profilePick?.data &&
                                                      cat?.attributes
                                                        ?.profilePick?.data[0]
                                                        ?.attributes.url
                                                        ? `${ASSETBASE}${
                                                            cat?.attributes &&
                                                            cat?.attributes
                                                              ?.profilePick &&
                                                            cat?.attributes
                                                              ?.profilePick
                                                              ?.data &&
                                                            cat?.attributes
                                                              ?.profilePick
                                                              ?.data[0]
                                                              ?.attributes.url
                                                          }`
                                                        : "/assets/images/pic2.png",
                                                    url: `${window.location.origin}/${cat.attributes.MachineSerial}/${cat.attributes.Slug}`,
                                                  });
                                                }}
                                              />
                                            </li>
                                            <li>
                                              <img
                                                src="/assets/images/plus.png"
                                                className="img-button"
                                                onClick={() => {
                                                  SetCompareCookies({
                                                    id: cat.id,
                                                    ...cat.attributes,
                                                  });
                                                  dispatch(getCompareList());
                                                }}
                                              />
                                            </li>{" "}
                                            <li>
                                              <img
                                                src={
                                                  CheckWishListProduct(
                                                    cat.id
                                                  ) == true
                                                    ? "/assets/images/wishlistadd.png"
                                                    : "/assets/images/add.png"
                                                }
                                                className="img-button"
                                                width={
                                                  CheckWishListProduct(cat.id)
                                                    ? 40
                                                    : "auto"
                                                }
                                                onClick={() => {
                                                  if (
                                                    CheckWishListProduct(
                                                      cat.id
                                                    ) == true
                                                  ) {
                                                    RemoveWishCookie(cat);
                                                    dispatch(getWishList());
                                                  } else {
                                                    SetWishCookies({
                                                      id: cat.id,
                                                      ...cat.attributes,
                                                    });
                                                    dispatch(getWishList());
                                                  }
                                                }}
                                              />
                                            </li>
                                          </ul>
                                        </div>
                                        <div>
                                          <p className="text-center upperText">
                                            get
                                            <br /> the best <br />
                                            price!
                                          </p>
                                        </div>
                                        <div>
                                          {/* <Button className="call-btn">
                                            <img
                                              className="me-2"
                                              src="/assets/images/call.png"
                                            />
                                            Call Us
                                          </Button> */}
                                          <a
                                            href="tel:+971581755868"
                                            className="call-btn"
                                          >
                                            <img
                                              className="me-2"
                                              src="/assets/images/call.png"
                                            />
                                            Call Us
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                  </Card.Body>
                                </Card>
                                // </Link>
                              ))}
                          {/* </InfiniteScroll> */}
                          {/* </div> */}
                        </Col>
                      </Row>
                    </Tab>

                    <Tab
                      eventKey="sale"
                      title={
                        activetab == "sale"
                          ? `For sale (${cataloglist.length})`
                          : "For sale"
                      }
                    >
                      <Row>
                        <Col sm={9}>
                          {tag !== "" && (
                            <div className="tags mb-4">
                              <div
                                className={
                                  tag == "Like New" ? "tagOther" : "tag"
                                }
                              >
                                {tag}{" "}
                                <img
                                  src="assets/images/kros.png"
                                  onClick={() => {
                                    dispatch(filterdata(""));
                                    dispatch(getdata());
                                  }}
                                />
                              </div>
                            </div>
                          )}
                          {/* <InfiniteScroll
                            dataLength={cataloglist && cataloglist?.length}
                            next={() => {
                              setPage(page + 1);
                              dispatch(setHasMore());
                              // setPageSize(pageSize + 6);
                            }}
                            // style={{
                            //   display: "flex",
                            //   flexDirection: "column-reverse",
                            // }} //To put endMessage and loader to the top.
                            endMessage={
                              <p style={{ textAlign: "center" }}>
                                <b>Yay! You have seen it all</b>
                              </p>
                            }
                            // inverse={true} //
                            hasMore={true}
                            loader={() => LoaderFunction()}
                            // scrollableTarget="scrollableDiv"
                          > */}
                            {cataloglist &&
                              cataloglist?.length > 0 &&
                              cataloglist?.map((cat) => (
                                // eslint-disable-next-line react/jsx-key
                                <Card>
                                  <Card.Body>
                                    <div
                                      className={`d-flex justify-content-between ${
                                        width < 575
                                          ? "card-content-mobile"
                                          : "card-content"
                                      }`}
                                    >
                                      <Link
                                        href={`/${cat.attributes.MachineSerial}/${cat.attributes.Slug}`}
                                        onClick={() => {
                                          dispatch(
                                            setQueryParameter({
                                              slug: cat.attributes.Slug,
                                              serialno:
                                                cat.attributes.MachineSerial,
                                            })
                                          );
                                        }}
                                        style={{
                                          display: "flex",
                                          justifyContent: "space-between",
                                        }}
                                      >
                                        <div>
                                          <Card.Img
                                            style={{ width: "240px" }}
                                            variant="top"
                                            src={
                                              cat?.attributes &&
                                              cat?.attributes?.profilePick &&
                                              cat?.attributes?.profilePick
                                                ?.data &&
                                              cat?.attributes?.profilePick
                                                ?.data[0]?.attributes.url
                                                ? `${ASSETBASE}${
                                                    cat?.attributes &&
                                                    cat?.attributes
                                                      ?.profilePick &&
                                                    cat?.attributes?.profilePick
                                                      ?.data &&
                                                    cat?.attributes?.profilePick
                                                      ?.data[0]?.attributes.url
                                                  }`
                                                : "/assets/images/pic2.png"
                                            }
                                          />
                                          <div className="tag-inner">
                                            <div className="tag1">
                                              {cat?.attributes?.SaleType}
                                            </div>
                                            <div className="tag2">
                                              {cat?.attributes?.Type}
                                            </div>
                                          </div>
                                        </div>
                                        <div className="description">
                                          <Card.Title className="mb-4">
                                            {cat?.attributes?.Title}
                                            <br />
                                          </Card.Title>
                                          <p>
                                            Working Hours:{" "}
                                            <strong style={{ fontSize: 12 }}>
                                              {" "}
                                              {cat?.attributes?.Workinghours}
                                            </strong>
                                          </p>
                                          <p>
                                            Operating Weight:{" "}
                                            <strong style={{ fontSize: 12 }}>
                                              {" "}
                                              {cat?.attributes?.Operatingweight}
                                            </strong>
                                          </p>
                                          <p>
                                            Bucket Capacity:
                                            <strong style={{ fontSize: 12 }}>
                                              {" "}
                                              {cat?.attributes?.bucketcapacity}
                                            </strong>
                                          </p>
                                          <p>
                                            Engine Power:
                                            <strong style={{ fontSize: 12 }}>
                                              {cat?.attributes?.EnginePower}
                                            </strong>
                                          </p>
                                        </div>
                                      </Link>
                                      <div className="d-flex flex-column justify-content-between align-items-center">
                                        <div className="icon-links">
                                          <ul>
                                            <li>
                                              <img
                                                src="/assets/images/share.png"
                                                className="img-button"
                                                onClick={() => {
                                                  onOpenModal2();
                                                  console.log(
                                                    `http://127.0.0.1:3003/${cat.attributes.MachineSerial}/${cat.attributes.Slug}`,
                                                    "url"
                                                  );
                                                  console.log(
                                                    window.location,
                                                    "location"
                                                  );
                                                  setshareurl({
                                                    title: cat.attributes.Title,
                                                    image:
                                                      cat?.attributes &&
                                                      cat?.attributes
                                                        ?.profilePick &&
                                                      cat?.attributes
                                                        ?.profilePick?.data &&
                                                      cat?.attributes
                                                        ?.profilePick?.data[0]
                                                        ?.attributes.url
                                                        ? `${ASSETBASE}${
                                                            cat?.attributes &&
                                                            cat?.attributes
                                                              ?.profilePick &&
                                                            cat?.attributes
                                                              ?.profilePick
                                                              ?.data &&
                                                            cat?.attributes
                                                              ?.profilePick
                                                              ?.data[0]
                                                              ?.attributes.url
                                                          }`
                                                        : "/assets/images/pic2.png",
                                                    url: `${window.location.origin}/${cat.attributes.MachineSerial}/${cat.attributes.Slug}`,
                                                  });
                                                }}
                                              />
                                            </li>
                                            <li>
                                              <img
                                                src="/assets/images/plus.png"
                                                className="img-button"
                                                onClick={() => {
                                                  SetCompareCookies({
                                                    id: cat.id,
                                                    ...cat.attributes,
                                                  });
                                                  dispatch(getCompareList());
                                                }}
                                              />
                                            </li>{" "}
                                            <li>
                                              <img
                                                src={
                                                  CheckWishListProduct(
                                                    cat.id
                                                  ) == true
                                                    ? "/assets/images/wishlistadd.png"
                                                    : "/assets/images/add.png"
                                                }
                                                className="img-button"
                                                width={
                                                  CheckWishListProduct(cat.id)
                                                    ? 40
                                                    : "auto"
                                                }
                                                onClick={() => {
                                                  SetWishCookies({
                                                    id: cat.id,
                                                    ...cat.attributes,
                                                  });
                                                  dispatch(getWishList());
                                                }}
                                              />
                                            </li>
                                          </ul>
                                        </div>
                                        <div>
                                          <p className="text-center upperText">
                                            get
                                            <br /> the best <br />
                                            price!
                                          </p>
                                        </div>
                                        <div>
                                          {/* <Button className="call-btn">
                                            <img
                                              className="me-2"
                                              src="/assets/images/call.png"
                                            />
                                            Call Us
                                          </Button> */}
                                          <a
                                            href="tel:+971581755868"
                                            className="call-btn"
                                          >
                                            <img
                                              className="me-2"
                                              src="/assets/images/call.png"
                                            />
                                            Call Us
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                  </Card.Body>
                                </Card>
                                // </Link>
                              ))}
                          {/* </InfiniteScroll> */}
                        </Col>
                      </Row>
                    </Tab>
                    <Tab eventKey="reserve" title="Reserved">
                      <Row>
                        <Col sm={9}>
                          {tag !== "" && (
                            <div className="tags mb-4">
                              <div
                                className={
                                  tag == "Like New" ? "tagOther" : "tag"
                                }
                              >
                                {tag}{" "}
                                <img
                                  src="assets/images/kros.png"
                                  onClick={() => {
                                    dispatch(filterdata(""));
                                    dispatch(getdata());
                                  }}
                                />
                              </div>
                            </div>
                          )}
                          {/* <InfiniteScroll
                            dataLength={cataloglist && cataloglist?.length}
                            next={() => {
                              setPage(page + 1);
                              dispatch(setHasMore());
                              // setPageSize(pageSize + 6);
                            }}
                            // style={{
                            //   display: "flex",
                            //   flexDirection: "column-reverse",
                            // }} //To put endMessage and loader to the top.
                            endMessage={
                              <p style={{ textAlign: "center" }}>
                                <b>Yay! You have seen it all</b>
                              </p>
                            }
                            // inverse={true} //
                            hasMore={true}
                            loader={() => LoaderFunction()}
                            // scrollableTarget="scrollableDiv"
                          > */}
                            {cataloglist &&
                              cataloglist?.length > 0 &&
                              cataloglist?.map((cat) => (
                                // eslint-disable-next-line react/jsx-key
                                <Card>
                                  <Card.Body>
                                    <div
                                      className={`d-flex justify-content-between ${
                                        width < 575
                                          ? "card-content-mobile"
                                          : "card-content"
                                      }`}
                                    >
                                      <Link
                                        href={`/${cat.attributes.MachineSerial}/${cat.attributes.Slug}`}
                                        onClick={() => {
                                          dispatch(
                                            setQueryParameter({
                                              slug: cat.attributes.Slug,
                                              serialno:
                                                cat.attributes.MachineSerial,
                                            })
                                          );
                                        }}
                                        style={{
                                          display: "flex",
                                          justifyContent: "space-between",
                                        }}
                                      >
                                        <div>
                                          <Card.Img
                                            style={{ width: "240px" }}
                                            variant="top"
                                            src={
                                              cat?.attributes &&
                                              cat?.attributes?.profilePick &&
                                              cat?.attributes?.profilePick
                                                ?.data &&
                                              cat?.attributes?.profilePick
                                                ?.data[0]?.attributes.url
                                                ? `${ASSETBASE}${
                                                    cat?.attributes &&
                                                    cat?.attributes
                                                      ?.profilePick &&
                                                    cat?.attributes?.profilePick
                                                      ?.data &&
                                                    cat?.attributes?.profilePick
                                                      ?.data[0]?.attributes.url
                                                  }`
                                                : "/assets/images/pic2.png"
                                            }
                                          />
                                          <div className="tag-inner">
                                            <div className="tag1">
                                              {cat?.attributes?.SaleType}
                                            </div>
                                            <div className="tag2">
                                              {cat?.attributes?.Type}
                                            </div>
                                          </div>
                                        </div>
                                        <div className="description">
                                          <Card.Title className="mb-4">
                                            {cat?.attributes?.Title}
                                            <br />
                                          </Card.Title>
                                          <p>
                                            Working Hours:{" "}
                                            <strong style={{ fontSize: 12 }}>
                                              {" "}
                                              {cat?.attributes?.Workinghours}
                                            </strong>
                                          </p>
                                          <p>
                                            Operating Weight:{" "}
                                            <strong style={{ fontSize: 12 }}>
                                              {" "}
                                              {cat?.attributes?.Operatingweight}
                                            </strong>
                                          </p>
                                          <p>
                                            Bucket Capacity:
                                            <strong style={{ fontSize: 12 }}>
                                              {" "}
                                              {cat?.attributes?.bucketcapacity}
                                            </strong>
                                          </p>
                                          <p>
                                            Engine Power:
                                            <strong style={{ fontSize: 12 }}>
                                              {cat?.attributes?.EnginePower}
                                            </strong>
                                          </p>
                                        </div>
                                      </Link>
                                      <div className="d-flex flex-column justify-content-between align-items-center">
                                        <div className="icon-links">
                                          <ul>
                                            <li>
                                              <img
                                                src="/assets/images/share.png"
                                                className="img-button"
                                                onClick={() => {
                                                  onOpenModal2();
                                                  console.log(
                                                    `http://127.0.0.1:3003/${cat.attributes.MachineSerial}/${cat.attributes.Slug}`,
                                                    "url"
                                                  );
                                                  console.log(
                                                    window.location,
                                                    "location"
                                                  );
                                                  setshareurl({
                                                    title: cat.attributes.Title,
                                                    image:
                                                      cat?.attributes &&
                                                      cat?.attributes
                                                        ?.profilePick &&
                                                      cat?.attributes
                                                        ?.profilePick?.data &&
                                                      cat?.attributes
                                                        ?.profilePick?.data[0]
                                                        ?.attributes.url
                                                        ? `${ASSETBASE}${
                                                            cat?.attributes &&
                                                            cat?.attributes
                                                              ?.profilePick &&
                                                            cat?.attributes
                                                              ?.profilePick
                                                              ?.data &&
                                                            cat?.attributes
                                                              ?.profilePick
                                                              ?.data[0]
                                                              ?.attributes.url
                                                          }`
                                                        : "/assets/images/pic2.png",
                                                    url: `${window.location.origin}/${cat.attributes.MachineSerial}/${cat.attributes.Slug}`,
                                                  });
                                                }}
                                              />
                                            </li>
                                            <li>
                                              <img
                                                src="/assets/images/plus.png"
                                                className="img-button"
                                                onClick={() => {
                                                  SetCompareCookies({
                                                    id: cat.id,
                                                    ...cat.attributes,
                                                  });
                                                  dispatch(getCompareList());
                                                }}
                                              />
                                            </li>{" "}
                                            <li>
                                              <img
                                                src={
                                                  CheckWishListProduct(
                                                    cat.id
                                                  ) == true
                                                    ? "/assets/images/wishlistadd.png"
                                                    : "/assets/images/add.png"
                                                }
                                                className="img-button"
                                                width={
                                                  CheckWishListProduct(cat.id)
                                                    ? 40
                                                    : "auto"
                                                }
                                                onClick={() => {
                                                  SetWishCookies({
                                                    id: cat.id,
                                                    ...cat.attributes,
                                                  });
                                                  dispatch(getWishList());
                                                }}
                                              />
                                            </li>
                                          </ul>
                                        </div>
                                        <div>
                                          <p className="text-center upperText">
                                            get
                                            <br /> the best <br />
                                            price!
                                          </p>
                                        </div>
                                        <div>
                                          {/* <Button className="call-btn">
                                            <img
                                              className="me-2"
                                              src="/assets/images/call.png"
                                            />
                                            Call Us
                                          </Button> */}
                                          <a
                                            href="tel:+971581755868"
                                            className="call-btn"
                                          >
                                            <img
                                              className="me-2"
                                              src="/assets/images/call.png"
                                            />
                                            Call Us
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                  </Card.Body>
                                </Card>
                                // </Link>
                              ))}
                          {/* </InfiniteScroll> */}
                        </Col>
                      </Row>
                    </Tab>
                    <Tab eventKey="sold" title="Recently Sold">
                      <Row>
                        <Col sm={9}>
                          {tag !== "" && (
                            <div className="tags mb-4">
                              <div className="tag">
                                {tag}{" "}
                                <img
                                  src="assets/images/kros.png"
                                  onClick={() => {
                                    dispatch(filterdata(""));
                                    dispatch(getdata());
                                  }}
                                />
                              </div>
                            </div>
                          )}
                          {/* <InfiniteScroll
                            dataLength={cataloglist && cataloglist?.length}
                            next={() => {
                              setPage(page + 1);
                              dispatch(setHasMore());
                              // setPageSize(pageSize + 6);
                            }}
                            // style={{
                            //   display: "flex",
                            //   flexDirection: "column-reverse",
                            // }} //To put endMessage and loader to the top.
                            endMessage={
                              <p style={{ textAlign: "center" }}>
                                <b>Yay! You have seen it all</b>
                              </p>
                            }
                            // inverse={true} //
                            hasMore={true}
                            loader={() => LoaderFunction()}
                            // scrollableTarget="scrollableDiv"
                          > */}
                            {cataloglist &&
                              cataloglist?.length > 0 &&
                              cataloglist?.map((cat) => (
                                // eslint-disable-next-line react/jsx-key
                                <Card>
                                  <Card.Body>
                                    <div
                                      className={`d-flex justify-content-between ${
                                        width < 575
                                          ? "card-content-mobile"
                                          : "card-content"
                                      }`}
                                    >
                                      <Link
                                        href={`/${cat.attributes.MachineSerial}/${cat.attributes.Slug}`}
                                        onClick={() => {
                                          dispatch(
                                            setQueryParameter({
                                              slug: cat.attributes.Slug,
                                              serialno:
                                                cat.attributes.MachineSerial,
                                            })
                                          );
                                        }}
                                        style={{
                                          display: "flex",
                                          justifyContent: "space-between",
                                        }}
                                      >
                                        <div>
                                          <Card.Img
                                            style={{ width: "240px" }}
                                            variant="top"
                                            src={
                                              cat?.attributes &&
                                              cat?.attributes?.profilePick &&
                                              cat?.attributes?.profilePick
                                                ?.data &&
                                              cat?.attributes?.profilePick
                                                ?.data[0]?.attributes.url
                                                ? `${ASSETBASE}${
                                                    cat?.attributes &&
                                                    cat?.attributes
                                                      ?.profilePick &&
                                                    cat?.attributes?.profilePick
                                                      ?.data &&
                                                    cat?.attributes?.profilePick
                                                      ?.data[0]?.attributes.url
                                                  }`
                                                : "/assets/images/pic2.png"
                                            }
                                          />
                                          <div className="tag-inner">
                                            <div className="tag1">
                                              {cat?.attributes?.SaleType}
                                            </div>
                                            <div className="tag2">
                                              {cat?.attributes?.Type}
                                            </div>
                                          </div>
                                        </div>
                                        <div className="description">
                                          <Card.Title className="mb-4">
                                            {cat?.attributes?.Title}
                                            <br />
                                          </Card.Title>
                                          <p>
                                            Working Hours:{" "}
                                            <strong style={{ fontSize: 12 }}>
                                              {" "}
                                              {cat?.attributes?.Workinghours}
                                            </strong>
                                          </p>
                                          <p>
                                            Operating Weight:{" "}
                                            <strong style={{ fontSize: 12 }}>
                                              {" "}
                                              {cat?.attributes?.Operatingweight}
                                            </strong>
                                          </p>
                                          <p>
                                            Bucket Capacity:
                                            <strong style={{ fontSize: 12 }}>
                                              {" "}
                                              {cat?.attributes?.bucketcapacity}
                                            </strong>
                                          </p>
                                          <p>
                                            Engine Power:
                                            <strong style={{ fontSize: 12 }}>
                                              {cat?.attributes?.EnginePower}
                                            </strong>
                                          </p>
                                        </div>
                                      </Link>
                                      <div className="d-flex flex-column justify-content-between align-items-center">
                                        <div className="icon-links">
                                          <ul>
                                            <li>
                                              <img
                                                src="/assets/images/share.png"
                                                className="img-button"
                                                onClick={() => {
                                                  onOpenModal2();
                                                  console.log(
                                                    `http://127.0.0.1:3003/${cat.attributes.MachineSerial}/${cat.attributes.Slug}`,
                                                    "url"
                                                  );
                                                  console.log(
                                                    window.location,
                                                    "location"
                                                  );
                                                  setshareurl({
                                                    title: cat.attributes.Title,
                                                    image:
                                                      cat?.attributes &&
                                                      cat?.attributes
                                                        ?.profilePick &&
                                                      cat?.attributes
                                                        ?.profilePick?.data &&
                                                      cat?.attributes
                                                        ?.profilePick?.data[0]
                                                        ?.attributes.url
                                                        ? `${ASSETBASE}${
                                                            cat?.attributes &&
                                                            cat?.attributes
                                                              ?.profilePick &&
                                                            cat?.attributes
                                                              ?.profilePick
                                                              ?.data &&
                                                            cat?.attributes
                                                              ?.profilePick
                                                              ?.data[0]
                                                              ?.attributes.url
                                                          }`
                                                        : `${window.location.origin}/assets/images/pic2.png`,
                                                    url: `${window.location.origin}/${cat.attributes.MachineSerial}/${cat.attributes.Slug}`,
                                                  });
                                                }}
                                              />
                                            </li>
                                            <li>
                                              <img
                                                src="/assets/images/plus.png"
                                                className="img-button"
                                                onClick={() => {
                                                  SetCompareCookies({
                                                    id: cat.id,
                                                    ...cat.attributes,
                                                  });
                                                  dispatch(getCompareList());
                                                }}
                                              />
                                            </li>{" "}
                                            <li>
                                              <img
                                                src={
                                                  CheckWishListProduct(
                                                    cat.id
                                                  ) == true
                                                    ? "/assets/images/wishlistadd.png"
                                                    : "/assets/images/add.png"
                                                }
                                                className="img-button"
                                                width={
                                                  CheckWishListProduct(cat.id)
                                                    ? 40
                                                    : "auto"
                                                }
                                                onClick={() => {
                                                  SetWishCookies({
                                                    id: cat.id,
                                                    ...cat.attributes,
                                                  });
                                                  dispatch(getWishList());
                                                }}
                                              />
                                            </li>
                                          </ul>
                                        </div>
                                        <div>
                                          <p className="text-center upperText">
                                            get
                                            <br /> the best <br />
                                            price!
                                          </p>
                                        </div>
                                        <div>
                                          {/* <Button className="call-btn">
                                            <img
                                              className="me-2"
                                              src="/assets/images/call.png"
                                            />
                                            Call Us
                                          </Button> */}
                                          <a
                                            href="tel:+971581755868"
                                            className="call-btn"
                                          >
                                            <img
                                              className="me-2"
                                              src="/assets/images/call.png"
                                            />
                                            Call Us
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                  </Card.Body>
                                </Card>
                                // </Link>
                              ))}
                          {/* </InfiniteScroll> */}
                        </Col>
                      </Row>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
        ;
        <Footer />
        {compareData && compareData.length > 0 && (
          <Container fluid>
            <Row
              style={{
                position: "fixed",
                bottom: 0,
                background: "#fff",
                width: "100%",
              }}
            >
              <Col md={{ span: 8 }}>
                <Row>
                  {compareData &&
                    compareData?.map((comp) => (
                      <Col
                        style={{
                          margin: "1rem",

                          // display: "flex",
                          // justifyContent: "flex-end",
                          // alignItems: "flex-end",
                        }}
                      >
                        <Row
                          style={{
                            border: "1px solid #e3e3e3",
                            position: "relative",
                            padding: ".8rem",
                            width: compareData.length > 1 ? "auto" : "40%",
                          }}
                        >
                          <Col md={2} style={{ marginRight: "1.2rem" }}>
                            <img
                              style={{ width: "60px" }}
                              variant="top"
                              src="/assets/images/jcb.png"
                            />
                          </Col>
                          <Col
                            md={9}
                            style={{ width: 150, fontWeight: "bold" }}
                            // style={{}}
                          >
                            {" "}
                            <p>{comp.Title}</p>
                          </Col>
                          <div
                            style={
                              width >= 1250
                                ? {
                                    position: "absolute",
                                    top: "-5%",
                                    left: "98.5%",
                                  }
                                : width >= 2250
                                ? {
                                    position: "absolute",
                                    top: "-5%",
                                    left: "96.5%",
                                  }
                                : {
                                    position: "absolute",
                                    top: "-5%",
                                    left: "96.5%",
                                  }
                            }
                          >
                            <img
                              src="/assets/images/cross.png"
                              height={20}
                              onClick={() => {
                                RemoveCompareCookie(comp);
                                dispatch(getCompareList());
                              }}
                            />
                          </div>
                        </Row>
                      </Col>
                    ))}
                </Row>
              </Col>
              <Col md={{ span: 1, offset: 2 }}>
                <Button
                  variant="default"
                  className="call-btn"
                  onClick={() => {
                    onOpenModal(true);
                  }}
                >
                  Compare{" "}
                </Button>
                <Button
                  variant="default"
                  className="call-btn-outline"
                  style={{ marginTop: 10 }}
                  onClick={() => {
                    Cookies.remove("compare");
                    dispatch(getCompareList());
                  }}
                >
                  Cancel
                </Button>
              </Col>
            </Row>
          </Container>
        )}
      </div>
      <Modal
        show={open}
        onHide={onCloseModal}
        centered
        styles={{
          modal: { width: "930px", animationFillMode: "forwards" },
          overlay: { animationFillMode: "forwards" },
        }}
        size={"lg"}
      >
        <Modal.Header closeButton>
          <Modal.Title>Compare Products</Modal.Title>
        </Modal.Header>
        <div className="recommend">
          <Container className="catalog-details catalog-modal py-5">
            <div className="products">
              <Row>
                {compareData &&
                  compareData.length > 0 &&
                  compareData.map((com) => (
                    <Col sm={4} md={4}>
                      <Card>
                        <Card.Body>
                          <div
                            className={`${
                              width < 575
                                ? "card-content-mobile"
                                : "card-content"
                            }`}
                          >
                            <div>
                              <Card.Img
                                variant="top"
                                src="/assets/images/new.png"
                              />
                              <div className="tag-inner">
                                <div className="tag1">{com?.SaleType}</div>
                                <div className="tag2">{com?.Type}</div>
                                <div className="tag3">
                                  <img src="/assets/images/addnew.png" />
                                </div>
                              </div>
                            </div>
                            <div className="">
                              <Card.Title
                                className="mb-3"
                                style={{ fontWeight: "bold" }}
                              >
                                <strong>{com.Title}</strong>
                              </Card.Title>
                              <div className="d-flex justify-content-center align-items-center">
                                {/* <Button className="call-btn">
                                   <img
                                     className="me-2"
                                     src="/assets/images/call.png"
                                   />
                                   Call Us
                                 </Button> */}
                                <a
                                  href="tel:+971581755868"
                                  className="call-btn"
                                >
                                  <img
                                    className="me-2"
                                    src="/assets/images/call.png"
                                  />
                                  Call Us
                                </a>
                              </div>
                            </div>
                          </div>
                          <div className="close-ico">
                            <img
                              className="me-2"
                              src="/assets/images/vecclose.png"
                              onClick={() => {
                                RemoveCompareCookie(com);
                                dispatch(getCompareList());
                                if (compareData.length == 0) {
                                  onCloseModal();
                                }
                              }}
                            />
                          </div>
                        </Card.Body>
                      </Card>
                    </Col>
                  ))}
              </Row>
              <div className="modal-table">
                <Table>
                  <tbody>
                    <tr>
                      <td>Reference Number</td>
                      {compareData &&
                        compareData.length > 0 &&
                        compareData.map((cm) => <td>SL-0181</td>)}
                    </tr>
                    <tr>
                      <td>Operating Weight</td>
                      {compareData &&
                        compareData.length > 0 &&
                        compareData.map((cm) => <td>{cm?.Operatingweight}</td>)}
                    </tr>{" "}
                    <tr>
                      <td>Mileage</td>
                      {compareData &&
                        compareData.length > 0 &&
                        compareData.map((cm) => <td>{cm?.Mileage} Km/h</td>)}
                    </tr>{" "}
                    <tr>
                      <td>Working Hours</td>
                      {compareData &&
                        compareData.length > 0 &&
                        compareData.map((cm) => <td>{cm?.Workinghours}</td>)}
                    </tr>{" "}
                    <tr>
                      <td>Engine Power</td>
                      {compareData &&
                        compareData.length > 0 &&
                        compareData.map((cm) => <td>{cm?.EnginePower}</td>)}
                    </tr>{" "}
                    <tr>
                      <td>Bucket Capacity</td>
                      {compareData &&
                        compareData.length > 0 &&
                        compareData.map((cm) => <td>{cm?.bucketcapacity}</td>)}
                    </tr>{" "}
                    <tr>
                      <td>Fuel Capacity</td>
                      {compareData &&
                        compareData.length > 0 &&
                        compareData.map((cm) => <td>{cm?.fuelcapacity}</td>)}
                    </tr>{" "}
                  </tbody>
                </Table>
              </div>
              <div className="d-flex justify-content-end mt-4">
                <Button className="done-btn" onClick={onCloseModal}>
                  Done
                </Button>
              </div>
            </div>
          </Container>
        </div>
      </Modal>
      <Modal
        show={open2}
        onHide={onCloseModal2}
        centered
        styles={{
          modal: {
            width: width < 575 ? "auto" : "100%",
            animationFillMode: "forwards",
          },
          overlay: { animationFillMode: "forwards" },
        }}
        size={"lg"}
      >
        <Modal.Header closeButton>
          <Modal.Title
            style={{
              padding: 20,
            }}
          >
            Share
          </Modal.Title>
        </Modal.Header>
        <div
          className=""
          style={{
            padding: 20,
          }}
        >
          <Container className="">
            <div
              className={`d-flex justify-content-between align-items-center ${
                width < 575 ? "card-content-mobile" : "card-content"
              }`}
            >
              <div>
                <Card>
                  <Card.Body>
                    <div
                      className={`${
                        width < 575 ? "card-content-mobile" : "card-content"
                      } d-flex`}
                    >
                      <div>
                        <Card.Img
                          style={{ width: "100px" }}
                          variant="top"
                          src="/assets/images/new.png"
                        />
                      </div>
                      <div className="">
                        <Card.Title className="mb-3 ms-2">
                          {shareObj.title}
                        </Card.Title>
                      </div>
                    </div>
                  </Card.Body>
                </Card>
              </div>
              <div className="socialLinks">
                <ul>
                  <li className="d-flex flex-column align-items-center">
                    <WhatsappShareButton
                      url={shareObj.url}
                      title={shareObj.title}
                    >
                      <WhatsappIcon />
                    </WhatsappShareButton>
                    Whatsapp
                  </li>
                  {console.log(shareObj, "shareObj")}
                  <li className="d-flex flex-column align-items-center">
                    <FacebookShareButton
                      url={shareObj.url}
                      title={shareObj.title}
                    >
                      <FacebookIcon />
                    </FacebookShareButton>
                    Facebook
                  </li>{" "}
                  <li className="d-flex flex-column align-items-center">
                    <TwitterShareButton
                      url={shareObj.url}
                      title={shareObj.title}
                    >
                      <TwitterIcon />
                    </TwitterShareButton>
                    Twitter
                  </li>
                  <li className="d-flex flex-column align-items-center">
                    <EmailShareButton url={shareObj.url} title={shareObj.title}>
                      <EmailIcon />
                    </EmailShareButton>
                    Email
                  </li>
                </ul>
              </div>
            </div>
            <div className="d-flex mt-4">
              <Form.Control
                type=""
                id="myInput"
                disabled
                value={shareObj.url}
              />
              <div>
                <Button
                  className="copy-btn ms-2"
                  onClick={() => {
                    // Get the text field
                    var copyText = document.getElementById("myInput");

                    // Select the text field
                    copyText.select();
                    copyText.setSelectionRange(0, 99999); // For mobile devices

                    // Copy the text inside the text field
                    window.navigator.clipboard.writeText(copyText.value);

                    // Alert the copied text
                    toast.success("Url copied");
                  }}
                >
                  Copy
                </Button>
              </div>
            </div>
          </Container>
        </div>
      </Modal>
    </>
  );
};
export default Catalog;
