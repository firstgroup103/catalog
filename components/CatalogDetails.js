/* eslint-disable @next/next/no-img-element */
/* eslint-disable react/jsx-key */
/* eslint-disable jsx-a11y/alt-text */
import {
  Container,
  Breadcrumb,
  Row,
  Col,
  Button,
  Table,
  Card,
  Form,
  Modal,
} from "react-bootstrap";
import {
  EmailShareButton,
  FacebookIcon,
  FacebookShareButton,
  TwitterIcon,
  TwitterShareButton,
  WhatsappIcon,
  WhatsappShareButton,
  EmailIcon,
} from "react-share";
import CatalogHeader from "./common/catalogheader";
import Slider from "react-slick";
import { useState, useEffect } from "react";
import { getOne } from "../libs/api";
import "react-responsive-modal/styles.css";
// import { Modal } from "react-responsive-modal";
import { useDispatch, useSelector } from "react-redux";
import {
  GetDetails,
  getRecentlyList,
  getWishList,
  getCompareList,
  setQueryParameter,
} from "../libs/store/slice";
import { useRouter } from "next/router";
import {
  SetRecentyCookies,
  SetCompareCookies,
  SetWishCookies,
  RemoveWishCookie,
  RemoveCompareCookie,
} from "@/utils/common";
import Cookies from "js-cookie";
import { useWindowSize } from "@/utils/usewindowsize";
import Footer from "./common/footer";
import { toast } from "react-toastify";

const ReadMore = ({ children }) => {
  const text = children;
  const [isReadMore, setIsReadMore] = useState(true);
  const toggleReadMore = () => {
    setIsReadMore(!isReadMore);
  };
  return (
    <p className="text">
      {isReadMore ? text && text.slice(0, 150) : text}
      <span onClick={toggleReadMore} className="read-or-hide">
        {isReadMore ? "...read more" : " show less"}
      </span>
    </p>
  );
};

const CatalogDetails = () => {
  const dispatch = useDispatch();
  const [open2, setOpenModal] = useState(false);
  const routers = useRouter();
  const [url, seturl] = useState("");
  const { catlogDetail, recentlyData, wishData, compareData, slug, serialno } =
    useSelector((state) => state.catalog);

  const onOpenModal2 = () => setOpenModal(true);
  const onCloseModal2 = () => setOpenModal(false);
  const CheckWishListProduct = (id) => {
    const Data = wishData.find((da) => da.id == id);
    console.log(Data, "dadad");
    if (Data !== undefined && Object.keys(Data).length > 0) {
      return true;
    } else {
      false;
    }
  };
  const { width } = useWindowSize();
  useEffect(() => {
    async function get(data, id) {
      console.log(routers, "hi vinay");
      // let serial, slu;
      if (serialno == "" || slug == "" || window !== "undefined") {
        dispatch(
          setQueryParameter({
            serialno: window.location.href.split("/")[3],
            slug: window.location.href.split("/")[4],
          })
        );
        seturl(window.location.href);
        console.log(window.location.href.split("/"), "ye kya tha");
      }

      const res = await dispatch(
        GetDetails({
          serialno:
            serialno == "" ? window.location.href.split("/")[3] : serialno,
          slug: slug == "" ? window.location.href.split("/")[4] : slug,
        })
      );
      console.log(res.payload, "dataOfCard");
      SetRecentyCookies({
        id: res?.payload?.data?.id,
        ...res?.payload?.data?.attributes,
      });
      if (Cookies.get("recently") !== undefined) {
        dispatch(getRecentlyList());
      }
    }
    get();
  }, [dispatch]);

  var settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
  };
  const [open, setOpen] = useState(false);

  const onOpenModal = () => setOpen(true);
  const onCloseModal = () => setOpen(false);
  return (
    <>
      <CatalogHeader />
      <Container className="mt-4 catalog-details">
        <div className="product-details">
          <div className="d-flex justify-content-between align-items-center">
            <div className="sale-tag">
              {catlogDetail[0]?.attributes?.SaleType}
            </div>
            <div className="d-flex flex-column justify-content-between align-items-center">
              <div className="icon-links">
                <ul>
                  <li>
                    <img
                      src="/assets/images/share.png"
                      className="img-button"
                      onClick={onOpenModal2}
                    />
                  </li>
                  <li>
                    <img
                      src="/assets/images/plus.png"
                      className="img-button"
                      onClick={() => {
                        SetCompareCookies({
                          id: catlogDetail[0].id,
                          ...catlogDetail[0].attributes,
                        });
                        dispatch(getCompareList());
                      }}
                    />
                  </li>{" "}
                  <li>
                    <img
                      src={
                        CheckWishListProduct(catlogDetail[0]?.id) == true
                          ? "/assets/images/wishlistadd.png"
                          : "/assets/images/add.png"
                      }
                      className="img-button"
                      width={
                        CheckWishListProduct(catlogDetail[0]?.id) ? 40 : "auto"
                      }
                      onClick={() => {
                        if (CheckWishListProduct(catlogDetail[0].id) == true) {
                          RemoveWishCookie(catlogDetail[0]);
                          dispatch(getWishList());
                        } else {
                          SetWishCookies({
                            id: catlogDetail[0].id,
                            ...catlogDetail[0].attributes,
                          });
                          dispatch(getWishList());
                        }
                      }}
                    />
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="">
            <h1>{catlogDetail[0]?.attributes?.Title}</h1>
            <Breadcrumb>
              <Breadcrumb.Item active href="#">
                Home
              </Breadcrumb.Item>
              <Breadcrumb.Item href="">Equipment</Breadcrumb.Item>
              <Breadcrumb.Item href="">
                {" "}
                {catlogDetail[0]?.attributes?.Title}
              </Breadcrumb.Item>
            </Breadcrumb>
          </div>
          <Row>
            <Col sm={6} md={6}>
                <div className='tag-inner'>
                  <div className="tag1">Like New</div>
                  <img
                      style={{ width: "100%" }}
                      src="/assets/images/image22.png"
                  />
                <div className="inner-tags">
                  <div className="d-flex inner-image">
                    <img style={{}} src="/assets/images/arroww.png" />
                    <img
                      style={{ marginLeft: ".4rem" }}
                      src="/assets/images/arrow1.png"
                    />
                  </div>
                  <div className="d-flex inner-image2">
                    <img style={{}} src="/assets/images/small1.png" />
                    <img
                      style={{ marginLeft: ".4rem" }}
                      src="/assets/images/small2.png"
                    />
                  </div>
                </div>
                <div className="mt-3 slider">
                  <Slider {...settings}>
                    <div className="">
                      <img src="/assets/images/jcb1.png" />
                    </div>
                    <div className="">
                      <img src="/assets/images/jcb1.png" />
                    </div>
                    <div>
                      <img src="/assets/images/jcb3.png" />
                    </div>
                    <div>
                      <img src="/assets/images/jcb3.png" />
                    </div>
                    <div>
                      <img src="/assets/images/jcb3.png" />
                    </div>{" "}
                    <div>
                      <img src="/assets/images/jcb3.png" />
                    </div>
                  </Slider>
                </div>
              </div>
            </Col>
            <Col sm={6} md={6}>
              <p>
                {catlogDetail[0]?.attributes && (
                  <ReadMore>
                    {catlogDetail[0]?.attributes?.Description}
                  </ReadMore>
                )}
              </p>
              <Row className="inner-col mt-4">
                <Col sm={6}>
                  <label>Working Hours</label>
                  <span>{catlogDetail[0]?.attributes?.Workinghours}</span>
                </Col>
                <Col sm={6}>
                  <label>Serial No.</label>
                  <span>{catlogDetail[0]?.attributes?.MachineSerial}</span>
                </Col>
              </Row>
              <div className="mt-4">
                {" "}
                {/* <Button className="call-btn">
                 <img
                   className="me-2"
                   src="/assets/images/call.png"
                 />
                 Call Us
               </Button> */}
                <a href="tel:+971581755868" className="call-btn">
                  <img className="me-2" src="/assets/images/call.png" />
                  Call Us
                </a>
              </div>
            </Col>
          </Row>
          <div className="specification">
            <h5>TECHNICAL SPECIFICATION</h5>
            {/* <img src="/assets/images/fff.png"/> */}
            <Table>
              <tbody>
                <tr>
                  <td>Reference Number</td>
                  <td>{catlogDetail[0]?.attributes?.refrenceno}</td>
                </tr>
                <tr>
                  <td>Working Hours</td>
                  <td>{catlogDetail[0]?.attributes?.Workinghours}</td>
                </tr>
                <tr>
                  <td>Operating Weight</td>
                  <td colSpan={2}>
                    {catlogDetail[0]?.attributes?.Operatingweight}
                  </td>
                </tr>
                <tr>
                  <td>Engine Power</td>
                  <td>{catlogDetail[0]?.attributes?.EnginePower}</td>
                </tr>{" "}
                <tr>
                  <td>Machine Serial</td>
                  <td>{catlogDetail[0]?.attributes?.MachineSerial}</td>
                </tr>{" "}
                <tr>
                  <td>Fuel Capacity</td>
                  <td>{catlogDetail[0]?.attributes?.FuelCapacity}</td>
                </tr>{" "}
                <tr>
                  <td>Bucket Capacity</td>
                  <td>{catlogDetail[0]?.attributes?.BucketCapacity}</td>
                </tr>{" "}
              </tbody>
            </Table>
          </div>
        </div>
      </Container>
      <div className="recommend">
        <Container className="catalog-details py-5">
          <div className="products">
            <h3 className="mb-4">Recommended Products</h3>
            <Row>
              <Col sm={4} md={4}>
                <Card>
                  <Card.Body>
                    <div
                      className={`${
                        width < 575 ? "card-content-mobile" : "card-content"
                      }`}
                    >
                      <div>
                        <Card.Img variant="top" src="/assets/images/new.png" />
                        <div className="tag-inner tag-inner-flex">
                          <div className="tag1">For Sale </div>
                          <div className="tag2">Used</div>
                          <div className="tag3">
                            <img src="/assets/images/addnew.png" />
                          </div>
                        </div>
                      </div>
                      <div className="">
                        <Card.Title className="mb-3">
                          2020 Shanding ZL26 Wheel Loader
                        </Card.Title>
                        <p>
                          Working Hours:<strong> 5,883</strong>
                        </p>
                        <p>
                          Operating Weight: <strong>6,000 Kg</strong>
                        </p>
                        <p>
                          Bucket Capacity: <strong>1.1 m3</strong>
                        </p>
                        <p>
                          Engine Power:<strong>42 kW @ 2400 rpm</strong>
                        </p>
                      </div>
                    </div>
                  </Card.Body>
                </Card>
              </Col>
              <Col sm={4} md={4}>
                <Card>
                  <Card.Body>
                    <div
                      className={`${
                        width < 575 ? "card-content-mobile" : "card-content"
                      }`}
                    >
                      <div>
                        <Card.Img variant="top" src="/assets/images/new.png" />
                        <div className="tag-inner tag-inner-flex">
                          <div className="tag1">For Sale </div>
                          <div className="tag2">Used</div>
                          <div className="tag3">
                            <img src="/assets/images/addnew.png" />
                          </div>
                        </div>
                      </div>
                      <div className="">
                        <Card.Title className="mb-3">
                          2020 Shanding ZL26 Wheel Loader
                        </Card.Title>
                        <p>
                          Working Hours:<strong> 5,883</strong>
                        </p>
                        <p>
                          Operating Weight: <strong>6,000 Kg</strong>
                        </p>
                        <p>
                          Bucket Capacity: <strong>1.1 m3</strong>
                        </p>
                        <p>
                          Engine Power:<strong>42 kW @ 2400 rpm</strong>
                        </p>
                      </div>
                    </div>
                  </Card.Body>
                </Card>
              </Col>{" "}
              <Col sm={4} md={4}>
                <Card>
                  <Card.Body>
                    <div
                      className={`${
                        width < 575 ? "card-content-mobile" : "card-content"
                      }`}
                    >
                      <div>
                        <Card.Img variant="top" src="/assets/images/new.png" />
                        <div className="tag-inner tag-inner-flex">
                          <div className="tag1">For Sale </div>
                          <div className="tag2">Used</div>
                          <div className="tag3">
                            <img src="/assets/images/addnew.png" />
                          </div>
                        </div>
                      </div>
                      <div className="">
                        <Card.Title className="mb-3">
                          2020 Shanding ZL26 Wheel Loader
                        </Card.Title>
                        <p>
                          Working Hours:<strong> 5,883</strong>
                        </p>
                        <p>
                          Operating Weight: <strong>6,000 Kg</strong>
                        </p>
                        <p>
                          Bucket Capacity: <strong>1.1 m3</strong>
                        </p>
                        <p>
                          Engine Power:<strong>42 kW @ 2400 rpm</strong>
                        </p>
                      </div>
                    </div>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </div>
          <div className="products mt-5 ">
            <h3 className="mb-4">Recently Viewed Products</h3>
            <Row
              style={{ marginLeft: recentlyData?.length > 0 ? "auto" : "3px" }}
            >
              {recentlyData?.length > 0
                ? recentlyData.map((item) => (
                    <Col sm={3} md={3}>
                      <Card>
                        <Card.Body>
                          <div
                            className={`${
                              width < 575
                                ? "card-content-mobile"
                                : "card-content"
                            }`}
                          >
                            <div>
                              <Card.Img
                                variant="top"
                                src="/assets/images/new.png"
                              />
                              <div className="tag-inner">
                                <div className="tag-add">
                                  <img src="/assets/images/addnew.png" />
                                </div>
                              </div>
                            </div>
                            <div className="">
                              <Card.Title className="mb-3">
                                {item.Title}
                              </Card.Title>
                            </div>
                          </div>
                        </Card.Body>
                      </Card>
                    </Col>
                  ))
                : "No Items Viewd Recently"}
            </Row>
          </div>
        </Container>
        <Footer />
        <div className="compare-div d-none">
          <Row>
            <Col sm={3} md={3}>
              <Card>
                <Card.Body>
                  <div className="d-flex">
                    <img className="me-3" src="/assets/images/compare1.png" />
                    <Card.Title>2020 Dynapac DFP10D Plate Compactor</Card.Title>
                  </div>
                </Card.Body>
                <div className="close-btn">
                  <img src="/assets/images/vecclose.png" />
                </div>
              </Card>
            </Col>
            <Col sm={3} md={3}></Col>
            <Col sm={3} md={3}></Col>
            <Col sm={3} md={3}>
              <div className="d-flex flex-column align-items-center">
                <Button className="compare-btn" onClick={onOpenModal}>
                  Compare
                </Button>
                <Button className="cancel-btn mt-3">Cancel</Button>
              </div>
            </Col>
          </Row>
        </div>
        <Modal
          show={open}
          onHide={onCloseModal}
          centered
          styles={{
            modal: { maxWidth: "930px" },
          }}
          size="lg"
        >
          <Modal.Header closeButton style={{ marginBottom: 10 }}>
            <Modal.Title>Compare Products</Modal.Title>
          </Modal.Header>
          <div className="recommend">
            <Container className="catalog-details catalog-modal py-5">
              <div className="products">
                <Row>
                  {compareData &&
                    compareData.length > 0 &&
                    compareData.map((com) => (
                      <Col sm={4} md={4}>
                        <Card>
                          <Card.Body>
                            <div
                              className={
                                width > 575
                                  ? "card-content-mobile"
                                  : "card-content"
                              }
                            >
                              <div>
                                <Card.Img
                                  variant="top"
                                  src="/assets/images/new.png"
                                />
                                <div className="tag-inner">
                                  <div className="tag1">{com?.SaleType}</div>
                                  <div className="tag2">{com?.Type}</div>
                                  <div className="tag3">
                                    <img src="/assets/images/addnew.png" />
                                  </div>
                                </div>
                              </div>
                              <div className="">
                                <Card.Title
                                  className="mb-3"
                                  style={{ fontWeight: "bold" }}
                                >
                                  <strong>{com.Title}</strong>
                                </Card.Title>
                                <div className="d-flex justify-content-center align-items-center">
                                  {/* <Button className="call-btn">
                                   <img
                                     className="me-2"
                                     src="/assets/images/call.png"
                                   />
                                   Call Us
                                 </Button> */}
                                  <a
                                    href="tel:+971581755868"
                                    className="call-btn"
                                  >
                                    <img
                                      className="me-2"
                                      src="/assets/images/call.png"
                                    />
                                    Call Us
                                  </a>
                                </div>
                              </div>
                            </div>
                            <div className="close-ico">
                              <img
                                className="me-2"
                                src="/assets/images/vecclose.png"
                              />
                            </div>
                          </Card.Body>
                        </Card>
                      </Col>
                    ))}
                </Row>
                <div className="modal-table">
                  <Table>
                    <tbody>
                      <tr>
                        <td>Reference Number</td>
                        {compareData &&
                          compareData.length > 0 &&
                          compareData.map((cm) => <td>SL-0181</td>)}
                      </tr>
                      <tr>
                        <td>Operating Weight</td>
                        {compareData &&
                          compareData.length > 0 &&
                          compareData.map((cm) => (
                            <td>{cm?.Operatingweight}</td>
                          ))}
                      </tr>{" "}
                      <tr>
                        <td>Mileage</td>
                        {compareData &&
                          compareData.length > 0 &&
                          compareData.map((cm) => <td>{cm?.Mileage} Km/h</td>)}
                      </tr>{" "}
                      <tr>
                        <td>Working Hours</td>
                        {compareData &&
                          compareData.length > 0 &&
                          compareData.map((cm) => <td>{cm?.Workinghours}</td>)}
                      </tr>{" "}
                      <tr>
                        <td>Engine Power</td>
                        {compareData &&
                          compareData.length > 0 &&
                          compareData.map((cm) => <td>{cm?.EnginePower}</td>)}
                      </tr>{" "}
                      <tr>
                        <td>Bucket Capacity</td>
                        {compareData &&
                          compareData.length > 0 &&
                          compareData.map((cm) => (
                            <td>{cm?.bucketcapacity}</td>
                          ))}
                      </tr>{" "}
                      <tr>
                        <td>Fuel Capacity</td>
                        {compareData &&
                          compareData.length > 0 &&
                          compareData.map((cm) => <td>{cm?.fuelcapacity}</td>)}
                      </tr>{" "}
                    </tbody>
                  </Table>
                </div>
              </div>
            </Container>
          </div>
        </Modal>
        <Modal
          show={open2}
          onHide={onCloseModal2}
          centered
          styles={{
            modal: {
              width: width < 575 ? "auto" : "100%",
              animationFillMode: "forwards",
            },
            overlay: { animationFillMode: "forwards" },
          }}
          size="lg"
        >
          <Modal.Header closeButton>
            <Modal.Title
              style={{
                padding: 20,
              }}
            >
              Share
            </Modal.Title>
          </Modal.Header>
          <div
            className=""
            style={{
              padding: 20,
            }}
          >
            <Container className="">
              <div
                className={`d-flex justify-content-between align-items-center ${
                  width < 575 ? "card-content-mobile" : "card-content"
                }`}
              >
                <div>
                  <Card>
                    <Card.Body>
                      <div
                        className={`${
                          width < 575 ? "card-content-mobile" : "card-content"
                        } d-flex`}
                      >
                        <div>
                          <Card.Img
                            style={{ width: "100px" }}
                            variant="top"
                            src="/assets/images/new.png"
                          />
                        </div>
                        <div className="">
                          <Card.Title className="mb-3 ms-2">
                            {catlogDetail[0]?.attributes.Title}
                          </Card.Title>
                        </div>
                      </div>
                    </Card.Body>
                  </Card>
                </div>
                <div className="socialLinks">
                  <ul>
                    <li className="d-flex flex-column align-items-center">
                      <WhatsappShareButton
                        url={url}
                        title={catlogDetail[0]?.attributes.Title}
                      >
                        <WhatsappIcon />
                      </WhatsappShareButton>
                      Whatsapp
                    </li>
                    <li className="d-flex flex-column align-items-center">
                      <FacebookShareButton
                        url={url}
                        title={catlogDetail[0]?.attributes.Title}
                      >
                        <FacebookIcon />
                      </FacebookShareButton>
                      Facebook
                    </li>{" "}
                    <li className="d-flex flex-column align-items-center">
                      <TwitterShareButton
                        url={url}
                        title={catlogDetail[0]?.attributes.Title}
                      >
                        <TwitterIcon />
                      </TwitterShareButton>
                      Twitter
                    </li>
                    <li className="d-flex flex-column align-items-center">
                      <EmailShareButton
                        url={url}
                        title={catlogDetail[0]?.attributes.Title}
                      >
                        <EmailIcon />
                      </EmailShareButton>
                      Email
                    </li>
                  </ul>
                </div>
              </div>
              <div className="d-flex mt-4">
                <Form.Control type="" id="myInput" disabled value={url} />
                <div>
                  <Button
                    className="copy-btn ms-2"
                    onClick={() => {
                      // Get the text field
                      var copyText = document.getElementById("myInput");

                      // Select the text field
                      copyText.select();
                      copyText.setSelectionRange(0, 99999); // For mobile devices

                      // Copy the text inside the text field
                      window.navigator.clipboard.writeText(copyText.value);

                      // Alert the copied text
                      toast.success("Url copied");
                    }}
                  >
                    Copy
                  </Button>
                </div>
              </div>
            </Container>
          </div>
        </Modal>
        {compareData && compareData.length > 0 && (
          <Container fluid>
            <Row
              style={{
                position: "fixed",
                bottom: 0,
                background: "#fff",
                width: "100%",
              }}
            >
              <Col md={{ span: 8 }}>
                <Row>
                  {compareData &&
                    compareData?.map((comp) => (
                      <Col
                        style={{
                          margin: "1rem",

                          // display: "flex",
                          // justifyContent: "flex-end",
                          // alignItems: "flex-end",
                        }}
                      >
                        <Row
                          style={{
                            border: "1px solid #e3e3e3",
                            position: "relative",
                            padding: ".8rem",
                            width: compareData.length > 1 ? "auto" : "40%",
                          }}
                        >
                          <Col md={2} style={{ marginRight: "1.2rem" }}>
                            <img
                              style={{ width: "60px" }}
                              variant="top"
                              src="/assets/images/jcb.png"
                            />
                          </Col>
                          <Col
                            md={9}
                            style={{ width: 150, fontWeight: "bold" }}
                            // style={{}}
                          >
                            {" "}
                            <p>{comp.Title}</p>
                          </Col>
                          <div
                            style={
                              width >= 1250
                                ? {
                                    position: "absolute",
                                    top: "-5%",
                                    left: "98.5%",
                                  }
                                : width >= 2250
                                ? {
                                    position: "absolute",
                                    top: "-5%",
                                    left: "96.5%",
                                  }
                                : {
                                    position: "absolute",
                                    top: "-5%",
                                    left: "96.5%",
                                  }
                            }
                          >
                            <img
                              src="/assets/images/cross.png"
                              height={20}
                              onClick={() => {
                                RemoveCompareCookie(comp);
                                dispatch(getCompareList());
                              }}
                            />
                          </div>
                        </Row>
                      </Col>
                    ))}
                </Row>
              </Col>
              <Col md={{ span: 1, offset: 2 }}>
                <Button
                  className="call-btn"
                  onClick={() => {
                    onOpenModal(true);
                  }}
                >
                  Compare{" "}
                </Button>
                <Button
                  className="call-btn-outline"
                  style={{ marginTop: 10 }}
                  onClick={() => {
                    Cookies.remove("compare");
                    dispatch(getCompareList());
                  }}
                >
                  Cancel
                </Button>
              </Col>
            </Row>
          </Container>
        )}
      </div>
    </>
  );
};
export default CatalogDetails;
