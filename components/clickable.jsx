import React from "react";
import { Card, CardBody } from "reactstrap";

export default function ClickableCard({ onClick }) {
  return (
    <Card onClick={onClick} style={{ cursor: "pointer" }}>
      <CardBody>This is a clickable card.</CardBody>
    </Card>
  );
}