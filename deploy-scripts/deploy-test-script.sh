#!/bin/bash

SERVER_IP=$SERVER_IP
SERVER_FOLDER="test1.defispace.com"

apt-get update -y
apt-get -y install rsync
yarn install --force
yarn run build

echo "Deploying to ${SERVER_FOLDER}"

ssh gitlab@${SERVER_IP} "cd /var/www/${SERVER_FOLDER} ; /home/gitlab/node_modules/pm2/bin/pm2 stop 'yarn dev -p 3004'"

ssh gitlab@${SERVER_IP} "rm -rf /var/www/${SERVER_FOLDER}/*"

rsync -avzh ./ gitlab@${SERVER_IP}:/var/www/${SERVER_FOLDER}/

echo "Finished copying files"

echo "Install packages and build project"
ssh gitlab@${SERVER_IP} "cd /var/www/${SERVER_FOLDER} ; /home/gitlab/node_modules/pm2/bin/pm2 start 'yarn dev -p 3004'"
